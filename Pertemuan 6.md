# **Bottom Navigation**

Sebelum membuat Bottom Navigation kita akan membuat class pembantu dan cutomisasi terlebih dahulu.

**1. Membuat Color Palette**

Pertama-tama kita akan membuat static class untuk menyimpan variabel warna yang akan kita pakai. Buat file **constant.dart** pada directory **lib.**

![image-20220418101808911](img\image-20220418101808911.png)

```dart
import 'package:flutter/material.dart';

class GojekPalette {
  static Color green = Color.fromARGB(255, 69, 170, 74);
  static Color grey = Color.fromARGB(255, 242, 242, 242);
  static Color grey200 = Color.fromARGB(200, 242, 242, 242);
  static Color menuRide = Color(0xffe99e1e);
  static Color menuCar = Color(0xff14639e);
  static Color menuBluebird = Color(0xff2da5d9);
  static Color menuFood = Color(0xffec1d27);
  static Color menuSend = Color(0xff8dc53e);
  static Color menuDeals = Color(0xfff43f24);
  static Color menuPulsa = Color(0xff72d2a2);
  static Color menuOther = Color(0xffa6a6a6);
  static Color menuShop = Color(0xff0b945e);
  static Color menuMart = Color(0xff68a9e3);
  static Color menuTix = Color(0xffe86f16);
}
```

Untuk membuat warna sendiri dapat menggunakan hexa atau argb.

**2. Membuat Custom Font**

Pertama-tama buat folder **fonts** pada halaman directory **assets** kemudian copy font yang akan kita pakai. Flutter support .ttf dan .otf font, sehingga kita bisa memakai keduanya. Download font berikut : (Neo Sans Font : https://www.cufonfonts.com/font/neo-sans-std). Kemudian extract pada folder **fonts**

![image-20220418102227485](img\image-20220418102227485.png)

Setelah itu daftarkan font tersebut pada file **pubspec.yaml** seperti berikut :

```dart
name: gojekapp
description: A new Flutter project.

# The following line prevents the package from being accidentally published to
# pub.dev using `flutter pub publish`. This is preferred for private packages.
publish_to: 'none' # Remove this line if you wish to publish to pub.dev

# The following defines the version and build number for your application.
# A version number is three numbers separated by dots, like 1.2.43
# followed by an optional build number separated by a +.
# Both the version and the builder number may be overridden in flutter
# build by specifying --build-name and --build-number, respectively.
# In Android, build-name is used as versionName while build-number used as versionCode.
# Read more about Android versioning at https://developer.android.com/studio/publish/versioning
# In iOS, build-name is used as CFBundleShortVersionString while build-number used as CFBundleVersion.
# Read more about iOS versioning at
# https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CoreFoundationKeys.html
version: 1.0.0+1

environment:
  sdk: ">=2.16.1 <3.0.0"

# Dependencies specify other packages that your package needs in order to work.
# To automatically upgrade your package dependencies to the latest versions
# consider running `flutter pub upgrade --major-versions`. Alternatively,
# dependencies can be manually updated by changing the version numbers below to
# the latest version available on pub.dev. To see which dependencies have newer
# versions available, run `flutter pub outdated`.
dependencies:
  flutter:
    sdk: flutter


  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^1.0.2

dev_dependencies:
  flutter_test:
    sdk: flutter

  # The "flutter_lints" package below contains a set of recommended lints to
  # encourage good coding practices. The lint set provided by the package is
  # activated in the `analysis_options.yaml` file located at the root of your
  # package. See that file for information about deactivating specific lint
  # rules and activating additional ones.
  flutter_lints: ^1.0.0

# For information on the generic Dart part of this file, see the
# following page: https://dart.dev/tools/pub/pubspec

# The following section is specific to Flutter.
flutter:

  # The following line ensures that the Material Icons font is
  # included with your application, so that you can use the icons in
  # the material Icons class.
  uses-material-design: true

  # To add assets to your application, add an assets section, like this:
  assets:
  - assets/img/img_gojek_logo.png
  #   - images/a_dot_ham.jpeg

  # An image asset can refer to one or more resolution-specific "variants", see
  # https://flutter.dev/assets-and-images/#resolution-aware.

  # For details regarding adding assets from package dependencies, see
  # https://flutter.dev/assets-and-images/#from-packages

  # To add custom fonts to your application, add a fonts section here,
  # in this "flutter" section. Each entry in this list should have a
  # "family" key with the font family name, and a "fonts" key with a
  # list giving the asset and other descriptors for the font. For
  # example:
  fonts:
    - family: NeoSans
      fonts:
        - asset: assets/fonts/Neo Sans Std Regular.otf
    - family: NeoSansBold
      fonts:
        - asset: assets/fonts/Neo Sans Std Bold.otf
    # - family: Trajan Pro
    #   fonts:
    #     - asset: fonts/TrajanPro.ttf
    #     - asset: fonts/TrajanPro_Bold.ttf
    #       weight: 700
  #
  # For details regarding fonts from package dependencies,
  # see https://flutter.dev/custom-fonts/#from-packages
```

Untuk menjadikan font tersebut sebagai font default dari aplikasi kita maka kita akan menambahkan atribut **fontFamily** pada **ThemeData** di file **main.dart** sesuai dengan nama font family yang kita buat di **pubspec.yaml.**

```dart
import 'package:flutter/material.dart';
import 'launcher/launcher_view.dart';
import 'constant.dart';
import 'package:flutter/services.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gojek',
      theme: new ThemeData(
        fontFamily: 'NeoSans',
        primaryColor: GojekPalette.green,
        accentColor: GojekPalette.green,
      ),
      home: new LauncherPage(),
    );
  }
}
```

**3. Membuat Bottom Navigation**

Sebelumnya buat halaman sebagai fragment dari halaman Landing Page, seperti Beranda, Pesanan, Inbox dan Akun. Pertama-tama buat dahulu halaman Beranda dengan membuat folder **beranda** pada directory **lib,** kemudian ikuti langkah yang sama seperti membuat halaman launcher dan landing page. buat class dengan nama **BerandaPage** dan **_BerandaPageState.**

Buat file **beranda_view.dart.dart**, 

![image-20220418103625550](img\image-20220418103625550.png)

edit seperti berikut:

```dart
import 'package:flutter/material.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => new _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold();
  }
}
```

kemudian edit **landingpage_view.dart** pada class _**LandingPageState** modifikasi code seperti berikut:

```dart
import 'package:flutter/material.dart';
import 'package:gojekapp/constant.dart';
import 'package:gojekapp/beranda/beranda_view.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => new _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new BerandaPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.home,
            color: Colors.grey,
          ),
          label: 'Beranda',
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.assignment,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.assignment,
            color: Colors.grey,
          ),
          label: 'Pesanan',
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.mail,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.mail,
            color: Colors.grey,
          ),
          label: 'Inbox',
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          label: 'Akun',
        ),
      ],
    );
  }
}
```

**`int _bottomNavCurrentIndex = 0;`** menyatakan posisi terakhir dari bottom navigation yang kita buat

**`List<Widget> _container = [ new BerandaPage(), ];`** berisi page sesuai dengan index dari **_bottomNavCurrentIndex**, jika kita mempunyai empat bottom navigation item maka seharusnya array tersebut berisi empat buah class yang mewakili setiap halaman pada bottom navigation item. SDK flutter sudah menyediakan generic material icon yang siap digunakan, sehingga untuk icon pada bottom navigation tidak perlu menambahkan secara manual.

Sehingga ketika code kita jalankan tampilan aplikasi akan menjadi sebagai berikut :

![image-20220418105023300](img\image-20220418105023300.png)

# **Beranda**

![img](img\berana1.png)

Pada halaman beranda akan kita bagi menjadi beberapa section, yaitu membuat **header**, **gopay menu**, **gojek menu**, **gofood menu**, **promo**. Dari section tersebut kita akan mempelajari untuk membuat **App Bar, Row, Column, GridView, Vertical Listview, Horizontal Listview, modal bottom sheet, dan FutureBuilder.**

**1. Membuat Custom App Bar**

![img](img\beranda2.png)

Flutter sudah menyediakan default app bar seperti yang terlihat pada bagian kiri, sedangkan untuk melakukan customisasi app bar maka kita harus membuat app bar sendiri sebagai child dari app bar bawaan flutter. 

Buat sebuah file bernama **beranda_gojek_appbar.dart** pada directory **beranda** kemudian buat sebuah class bernama **GojekAppBar extends AppBar**, kemudian modfikasi class tersebut seperti berikut :

```dart
import 'package:flutter/material.dart';

class GojekAppBar extends AppBar {
  GojekAppBar()
      : super(
            elevation: 0.25,
            backgroundColor: Colors.white,
            flexibleSpace: _buildGojekAppBar());

  static Widget _buildGojekAppBar() {
    return new Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Image.asset(
            "assets/img/img_gojek_logo.png",
            height: 50.0,
            width: 100.0,
          ),
          new Container(
            child: new Row(
              children: <Widget>[
                new Container(
                  height: 28.0,
                  width: 28.0,
                  padding: EdgeInsets.all(6.0),
                  decoration: new BoxDecoration(
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(100.0)),
                      color: Colors.orangeAccent),
                  alignment: Alignment.centerRight,
                  child: new Icon(
                    Icons.local_bar,
                    color: Colors.white,
                    size: 16.0,
                  ),
                ),
                new Container(
                  padding: EdgeInsets.all(6.0),
                  decoration: new BoxDecoration(
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(5.0)),
                      color: Color(0x50FFD180)),
                  child: new Text(
                    "1.781 poin",
                    style: TextStyle(fontSize: 14.0),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
```

Setelah membuat custom appbar maka panggil class tersebut sebagai app bar pada halaman beranda(**beranda_view.dart**). Seperti berikut :

```dart
import 'package:flutter/material.dart';
import 'package:gojekapp/beranda/beranda_gojek_appbar.dart';


class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => new _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new GojekAppBar(),
      body: new Container(),
    );
  }
}
```

Namun ketika code tersebut di run letak app bar tidak proprosional pada device tertentu (seperti iphone X) seperti yang kita lihat dibawah ini.

![img](img\berana3.png)

Untuk menanggulangi hal tersebut kita akan menambahkan safe area sebagai container utamanya, hal ini dimaksudkan untuk mencegah container tampilan yang kita buat terlalu menjorok ke sisi tertentu pada beberapa smartphone, seperti pada iPhone X, safe area dimaksudkan untuk memberikan space pada poni, tombol gesture home, atau pada saat aplikasi lanscape sehingga tidak terpotong. Untuk menambahkan safe area modifikasi code sebagai berikut(**beranda_view.dart**) :

```dart
import 'package:flutter/material.dart';
import 'package:gojekapp/beranda/beranda_gojek_appbar.dart';


class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => new _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
        child: Scaffold(
      appBar: new GojekAppBar(),
      body: new Container(),
    ));
  }
}
```

Sehingga ketika di run app bar akan berada pada posisi yang seharusnya.

![image-20220418110331004](img\image-20220418110331004.png)
