# App Bar dan Properties

## Basic AppBar Widget di Flutter

Umumnya sebuah dalam sebuah basic Appbar widget tersusun dari tiga element utama yaitu **Leading**, **Title** dan **Actions**.

![membuat AppBar pada Flutter](img\basic-elemen-appbar.png)

#### **Leading**

widget yang tampil dibagian sisi kiri menu sebelum title yang umumnya difungsikan untuk menambahkan tombol navigasi [drawer](https://api.flutter.dev/flutter/material/Drawer-class.html) atau navigasi lainnya.

```dart
appBar: AppBar(
   leading: Icon(Icons.home),
```

Leading pada AppBar bersifat opsional, sehingga sebuah Appbar tidak memiliki leading pun tidak akan menjadi error. Dan apabila leading null maka widget yang berada disebelah kanannya (title widget) akan otomatis memenuhi bagian dari leading section.

#### **Title**

Merupakan widget utama pada sebuah AppBar. Biasanya akan diisi oleh Text widget atau gambar logo sebuah aplikasi. Properti title pada AppBar seperti **child** pada *[single child widget](https://belajarflutter.com/category/flutter-widgets/single-child-widget/)* jadi kita dapat memasukan widget-widget lain sesuai kebutuhan. Meskipun title merupakan properti utama pada appBar namun sifatnya pun bersifat opsional.

Penggunaan title pada AppBar menggunakan Text widget

```dart
appBar: AppBar(
   title: Text('Ini Title AppBar'),
```

Contoh title Appbar menggunakan Image

```dart
appBar: AppBar(
   title: Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTybCniORbVSJJgvqcMaMwxSY4IL6OHxKftgQ&usqp=CAU'),
```

![Screen Shot 2020 08 03 at 20.11.54](img\Screen-Shot-2020-08-03-at-20.11.54.png)

#### **Actions**

Merupakan property yang berfungsi untuk menambahkan aksi berupa widget dibagian sisi kanan menu yang dapat menampilkan multiple widget lainnya secara horizontal seperti column widget. Action ini biasanya digunakan untuk menampilkan tombol menu yang sering digunakan seperti tombol search, profile dan popup menu.

```dart
appBar: AppBar(
  actions: <Widget>[
    IconButton(icon: new Icon(Icons.call, color: Colors.white)),
    IconButton(icon: new Icon(Icons.search, color: Colors.white)),
  ],
```

Selain tiga properti diatas, Appbar juga memiliki properti lain yang tidak kalah pentingnya yaitu :

#### **Background Color.**

Secara default AppBar widget menggunakan warna biru sebagai backgroundnya namun anda bisa menggantinya dengan warna custom dengan menambahan :

```dart
backgroundColor: Colors.redCopy
```

Kita juga dapat menggunakan nilai ***transparent\*** untuk membuat transparan pada background appbar flutter.

```dart
backgroundColor: Colors.transparent,Copy
```

Apabila ingin merubah warna background Appbar menjadi warna gradasi maka kita menggunakan flexibleSpace dan BoxDecoration. Codenya seperti dibawah ini :

```dart
appBar: AppBar(
  title: Text('BelajarFlutter.com'),
  flexibleSpace: Container(
    decoration: BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: <Color>[
        Colors.green,
        Colors.blue
      ])          
    ),
  ),     
),
```

![gradient appbar flutter](img\gradient-appbar-flutter.png)

#### **Elevation.**

Jika Anda melihatnya pada bagian menu terdapat bayangan dibagian bawah menu, property elevation berguna untuk mengatur bayangannya. Secara default bayangannnya memiliki nilai 4, jadi apabila ingin menghapus bayangan ganti dengan nilai 0 dengan kode berikut :

```dart
elevation: 0,Copy
```

#### **centerTitle.**

Fungsi property ini adalah untuk membuat sebuah judul di AppBar dapat diposisikan ke bagian tengah, untuk mengaktifkanya tinggal berikan fungsi true di property ini. Contohnya :

```dart
centerTitle: true,Copy
```

Karena pada dasarnya judul yang tidak menggunakan property ini akan berada di bagian sisi kiri. Fungsinya juga sama dengan property Alignment.

#### **Brightness.**

Secara default warna item (Text dan Icon) pada AppBar adalah berwarna putih. Lalu bagaimana jika kita membutuhkan warna putih atau terang sebagai background pada Appbar? Icon dan text menjadi kurang terlihat dan bahkan tidak terlihat. Text dan Icon yang dimaksud disini yaitu secara keseluruhan yaitu widget pada leading, title, action bahkan simbol wifi, baterai dan jam.

Untuk widget tentu kita bisa rubah seperti widget pada umumnya, bisa memberi warna text dan lain-lain. Akan tetapi bagaimana dengan icon yang berada diluar safeArea seperti icon wifi, baterai, jam, dan lain-lain ?

Untuk merubahnya kita dapat menggunakan property **Brightness**. Saat kita memiliki background yang cenderung gelap maka anda dapat menggunakan `brightness: Brightness.light,` yang artinya icon di luar safe area akan menjadi berwarna terang (putih). Dan begitu juga sebaliknya, apabila background appBar cenderung terang maka gunakan `brightness: Brightness.dark,`

```dart
appBar: AppBar(
  brightness: Brightness.light,
  backgroundColor: Colors.yellowAccent,
  title: Text(
    'BelajarFlutter.com',
    style: TextStyle(color: Colors.purple)
  )
),
```

Apabila anda pernah mendengar istilah mode malam, property Brightness juga dapat sangat berguna saat pergantian mode theme. FYI android versi 10 (API level 29) atau lebih tinggi sudah support dengan mode malam (dark mode).

#### **titleSpacing.**

Bertujuan untuk memberikan jarak spasi pada judul agar bisa lebih jauh atau lebih dekat dengan leading. Property ini hanya akan memberikan perubahan jarak secara garis horizontal saja. Semakin besar nilainya maka judul akan menjauh dari Leading.

Contoh penggunaan kodenya sebagai berikut :

```
titleSpacing: 12,Copy
```

#### **toolbarOpacity**

Yaitu berguna untuk mengatur transaparasi menu AppBar aplikasi anda. Penggunaan Property ini hanya bisa menggunakan nilai 1.0 untuk tetap bewarna dan 0.0 untuk 100% transparan.

Sedikit informasi, penggunaan property ini akan sedikit mengganggu jika digunakan di Widget ScaffoldAppBar karena akan membuat menu menjadi transparan. Property ini sering digunakan di Widget SliverAppBar karena memang dikondisikan Ketika layar aplikasi digulir ke bagian bawah.

Namun jika ingin mencoba hasilnya gunakan kode ini :

```
toolbarOpacity: 0.0,Copy
```

Itulah beberapa penerapan fungsi property di AppBar Class yang bisa Anda gunakan semuanya dengan mengkombinasikan masing masing property untuk mendapatkan tampilan AppBar lebih menarik.

# Cara Membuat App Bar

1. Ubah file main.dart menjadi seperti berikut

   ```dart
   import 'package:flutter/material.dart';
   
   void main() => runApp(MyApp());
   
   class MyApp extends StatelessWidget {
   
     // This widget is the root of your application.
     @override
     Widget build(BuildContext context) {
       return MaterialApp(
         title: 'Jor Flutter',
         theme: ThemeData(
           primarySwatch: Colors.blue,
         ),
         home: Scaffold(
           body: SafeArea(
             child: Text("Flutter Pertama"),
           ),
         ),
       );
     }
   }
   ```

2. diatas tag `body: SafeArea(...)` kita tambah kan appBar.

   ```dart
   appBar: AppBar(
             title: Text("Cashy"),
           ),
   ```

   Jalankan project flutter `F5` 

   ![image-20220325205246361](img\image-20220325205246361.png)

3. Kita coba ubah warna backgroundnya. Dibawah tag `title: Text("Cashy"),` tambahkan

   ```dart
   backgroundColor: Colors.red,
   ```

    Untuk merubah warna background menjadi merah

   ![image-20220325205614258](img\image-20220325205614258.png)

​		Untuk lebih lengkapnya kita bisa membukan doumentasi dari officialflutter https://docs.flutter.dev/

4. Sekarang kita coba menambahkan button pada **AppBar** kita. Tambahkan code berikut dibawah `return MaterialApp(` untuk menghilangkan tulisan debug 

   ```dart
   debugShowCheckedModeBanner: false,
   ```

   Dibawah `backgroundColor: Colors.red,` kita tambahkan kode berikut

   ```dart
   actions: <Widget>[
               IconButton(
                 onPressed: () {},
                 icon: Icon(Icons.mail),
                 color: Colors.yellowAccent,
               )
             ],
   ```

   ![image-20220325210731225](img\image-20220325210731225.png)

## Import Image

1. Download gambar dan masukkan kefolder **assets>images**

   ![image-20220325211244767](img\image-20220325211244767.png)

2.  Aktifkan asset pada **pubspec.yaml**,

   ![image-20220325211646944](img\image-20220325211646944.png)

​		**aseets** harus rata dengan **uses-material-design**!

3. Ubah tag `body:`

   ```dart
           body: SafeArea(
               child: Image(
                 image: AssetImage('assets/images/gambar.png'),
                 height: 200,
           )),
   ```

   

   ![image-20220325212623820](img\image-20220325212623820.png)

## Container Class

Dalam pembuatan sebuah aplikasi, layout sangat menjadi bagian penting atau bagian dasar sebelum aplikasi tersebut diberikan isi dan desain. Membuat sebuah layout aplikasi di Flutter bertujan agar bisa meletakkan dan memposisikan tata letak seperti teks dan tombol atau bagian lainnya sesuai yang diinginkan.

![Container Class pada Flutter](img\penjelasan-container-widget.png)

Container bisa diartikan Bahasa Indonesia sebagai wadah yang dimana dapat menyimpan berbagai macam attribute dan menampung berbagai macam fungsi objek yang membuat kotak container bisa menampilkan berbagai macam efek dan hasil didalamnya.

Container widget pada flutter merupakan “***Single Child Widget\***” yang berarti hanya dapat memiliki satu buah child widget saja. Akan tetapi dalam sebuah container kita dapat menempatkan row, column, text atau bahkan container lain. Container widget juga dapat dijadikan sebagai dasar dan serangkaian awal dari suatu layout aplikasi.

![sample flutter layout 46c76f6ab08f94fa4204469dbcf6548a968052af102ae5a1ae3c78bc24e0d915](img\sample-flutter-layout-46c76f6ab08f94fa4204469dbcf6548a968052af102ae5a1ae3c78bc24e0d915.png)

### Penerapan Fungsi Property di Widget Container.

#### 1. Property Child.

Seperti yang telah disampaikan sebelumnya bawah Container merupakan *Single Child Wid*get. Ciri utama dari tipe tersebut yaitu memilik properti **child**. Pada properti ini digunakan untuk memuat anakan atau turunan dari Container, yang dapat memuat widget lainnya seperti Text, Column, ListView, dll.

Berikut contoh penerapan penggunaan property child dalam Widget Container Class.

```dart
body: Container(
  child: Text(
    'Ayo Belajar Flutter',
  )
),
```

#### 2. Property Alignment.

Dengan container widget kita dapat mengatur posisi child widget menggunakan fungsi property Alignment diantaranya seperti :

- bottomCenter untuk memindahakan kebawah bagian tengah.
- bottomLeft untuk memindahkan ke bawah bagian Kiri.
- bottomRight untuk memindahkan ke bawah bagian Kanan.
- center untuk untuk memindahkan ke posisi tengah.
- centerLeft untuk memindahkan ke tengah bagian Kiri.
- centerRight untuk memindahkan ke tengah bagian Kanan.
- topCenter untuk memindahkan ke atas bagian Tengah.
- topLeft untuk memindahkan ke atas bagian Kiri.
- topRight untuk memindahkan ke atas bagian Kanan.

Untuk penerapan dan pemasangan fungsi Alignment pada container dapt dilakukan seperti kode dibawah ini :

```dart
body: Container(
  alignment: Alignment.bottomCenter,
  child: Text(
    'Ayo Belajar Flutter',
    style: TextStyle(
      fontSize: 20,
    ),
  )
),
```

![penggunaan container alignment](img\penggunaan-container-alignment.png)

#### 3. Property Color

Penambahan property ini untuk membuat wadah / Container memiliki warna latar belakang menyesuaikan dengan keseluruhan bagiannya. Contoh kodenya :

```dart
body: Container(
  alignment: Alignment.center,
  color: Colors.purple,
  child: Text(
    'Ayo Belajar Flutter',
    style: TextStyle(
      fontSize: 20,
      color: Colors.white
    ),
  )
),
```

![Screen Shot 2020 08 02 at 08.43.31](img\Screen-Shot-2020-08-02-at-08.43.31-1024x798.png)

Apabila Anda menginginkan warna dengan custom bisa menggunakan kode berikut :

```
color: Color(0xFF42A5F5);
color: Color.fromARGB(0xFF, 0x42, 0xA5, 0xF5);
color: Color.fromARGB(255, 66, 165, 245);
color: Color.fromRGBO(66, 165, 245, 1.0);
```

Tinggal sesuaikan dengan kode warna pilihan yang diinginkan.

#### 4. Property Height dan Weight.

Karena secara default ukuran container akan menyesuaikan dengan body layar aplikasinya maka agar bisa disesuaikan layoutnya bisa diatur dengan menambahkan properti tinggi dan lebarnya.

Karena **height** dan **weight** merupakan 2 property yang berbeda maka pastikan gunakan tanda koma setiap akan menambah property baru di dalam Widget Container.

#### 5. Property Margin.

Margin digunakan untuk membuat jarak diantara sisi container dengan widget lainnya sehingga container akan menjorok ke bagian dalam. Property margin bisa digunakan sekaligus kepada 4 sisi wadah dengan menggunakan fungsi EdgeInsets.all().

Penerapan margin dalam Container :

```dart
body: Container(
  margin: EdgeInsets.all(20),
  height: 200,
  width: 200,
  alignment: Alignment.topLeft,
  color: Colors.purple,
  child: Text(
    'Ayo Belajar Flutter',
    style: TextStyle(
      fontSize: 20,
      color: Colors.white
    ),
  )
),
```

Ukuran margin menggunakan satuan pixel. Apabila anda ingin merubah ukuran margin pada sisi tertentu bisa menggunakan kode EdgeInsets.only() sebagai contoh :

```
margin: EdgeInsets.only(left: 20)
```

#### 6. Property Padding

Digunakan untuk menambahkan jarak antara container dengan widget yang ada dalamnya. Sama dengan margin penggunaannya bisa dengan menambahakan fungsi EdgeInsets.all() untuk mengatur ukuran padding sama di ke-empat sisinya.

```dart
body: Container(
  padding: EdgeInsets.only(left:20),
  height: 200,
  width: 200,
  alignment: Alignment.topLeft,
  color: Colors.purple,
  child: Text(
    'Ayo Belajar Flutter',
    style: TextStyle(
      fontSize: 20,
      color: Colors.white
    ),
  )
),
```

![Screen Shot 2020 08 02 at 09.38.11](img\Screen-Shot-2020-08-02-at-09.38.11-1024x455.png)

#### 7. Property Transform.

Difungsikan untuk melakukan rotasi pada wadah (container) yang dapat dilakukan dengan melakukan dari berbagai sumbu putar misalnya X,Y dan Z yang dimana kita menggunakan fungsi **Matrix4** untuk melakukan rotasinya.

Contoh penerapannya seperti demikian.

```dart
Container(
  margin: EdgeInsets.all(20),
  transform: Matrix4.rotationZ(0.1),
  height: 200,
  width: 200,
  color: Colors.purple,
),
```

Dalam fungsi diatas menyatakan bahwa Container diputar pada sumbu Z di titik 0.1

![Screen Shot 2020 08 02 at 09.42.04](img\Screen-Shot-2020-08-02-at-09.42.04-300x257.png)

Untuk Mengganti sumbu putarnya tinggal ubah pada bagian rotationZ menjadi rotationX atau rotationY.

#### 8. Property Decoration

Yang terakhir disini Anda bisa menghias kotak Container dengan berbagai macam efek dekorasi seperti misalnya mengubah warna border, memberikan gambar, atau membuat efek bayangan pada kotak containernya.

Ada banyak efek lainnya yang bisa anda tambahkan di sini yang menggunakan fungsi decoration: BoxDecoration()

Pemakaian kodenya seperti ini.

```dart
Container(
  decoration: BoxDecoration(
    color: const Color(0xff7c94b6),
    image: const DecorationImage(
      image: NetworkImage('https://i.pinimg.com/originals/91/86/6b/91866b918c9cca0747f483a46943e926.jpg'),
      fit: BoxFit.cover,
    ),
    border: Border.all(
      color: Colors.black,
      width: 8,
    ),
    borderRadius: BorderRadius.circular(12),
  ),
  height: 280,
  width: 200,
  margin: EdgeInsets.all(20)
)
```

![container decoration 1](img\container-decoration-1.png)

Dari kode diatas ada beberapa penambahan efek dekorasi pada Container diantaranya yaitu pemberian warna border dan menginputkan gambar kedalam Container. Dimana gambar tersebut bersumber dari link gambar di internet yang dimana dibutuhkan fungsi **NetworkImage** sebagai pemanggilan.

Latihan!

Kita ubah `body:`

```dart
body: SafeArea(
  child: Container(
    margin: EdgeInsets.all(40),
    child: Column(
      children: <Widget>[
        Image(
          image: AssetImage('assets/images/gambar.png'),
          height: 200,
        ),
        Text("Rich Together"),
        Text("Save your money little bit and we will help to be rich", textAlign: TextAlign.center,)
      ],
    ),
  ),
),
```

![image-20220325214415837](img\image-20220325214415837.png)