# Struktur Folder dan File pada Flutter

Dalam pembuatan project baru, bisa kita lihat bahwa struktur folder dan file pada flutter cukup simpel. Bagi yang belum tahu apa itu direktori, `direktori` sama artinya dengan `folder`, jadi kita mungkin akan menggunakan kedua kata tersebut. kita akan jelaskan, masing-masing kegunaan dari direktori dan file yang dibuat otomatis dan akan sedikit kita tambahin sesuai pengalaman kita.

## Direktori atau Folder

### .dart_tool

![image-20220318200038824](img\image-20220318200038824.png)

Muncul pada `Dart 2`, digunakan oleh pub dan tools lainnya. Direktori ini menggantikan direktori `.pub` setelah SDK 2.0.0-dev.32.0 rilis. Tidak perlu mengubah isi direktori ini. Karena kita sudah pernah menjalankan program melalui emulator yang ada. Maka secara ototmatis flutter juga akan merecordnya.

> Kita tidak perlu merubah apapun dalam folder ini

### .idea

![image-20220318200458856](img\image-20220318200458856.png)

Perlu kita ketahui terlebih dahulu, pada Intellij IDEA terdapat dua tipe format untuk konfigurasi Project disimpan, yakni File-based format dan Directory-based format. Flutter menganut yang kedua (Directory-based format). Nah, di direktori `.idea` ini setiap pengaturan Project disimpan secara spesifik dalam bentuk `xml`. Pada sistem operasi macOS dan Linux, direktori `.idea` akan ter hidden secarta default. Kita tidak perlu mengubah-ubah isi dari direktori ini. Jika menggunakan git, direktori `.idea/` akan ditulis pada `.gitignore` secara otomatis.

### android

![image-20220318200644961](img\image-20220318200644961.png)

Merupakan Folder yang penting, isi dari folder ini diperlukan untuk proses build menjadi aplikasi native ke `platform android`. Didalamnya ada file `build.gradle`, disini tempat untuk menentukan versi Sdk dari aplikasi. Dan pada file `AndroidManifest.xml` berguna untuk mengatur permissions, untuk meminta ijin menggunakan camera, file explorer, sound recording, dsb. Folder utama didalam android adalah folder main.

Folder ini merupakan tempat dimana pengaturan **khusus** terhadap aplikasi android kita nanti. 

### ios

![image-20220318200906556](img\image-20220318200906556.png)

Fungsinya sama seperti folder android, folder ini diperlukan jika kita ingin menjalankan aplikasi ke `platform iOS`. Walaupun dalam penerapannya, kita memerlukan sistem operasi macOS dengan XCode nya agar bisa build menjadi aplikasi ke iPhone. Jika kita tidak memiliki device nya, kita bisa menyewa Cloud Service yang menggunakan macOS.

### lib

![image-20220318201010898](img\image-20220318201010898.png)

Ini adalah folder utama, dan merupakan code dari aplikasi Flutter mu. Pada awalnya, akan ada file `main.dart` yang dibuatkan secara otomatis. File main.dart adalah file root dari aplikasi Flutter mu, jadi kita beri saran untuk kita membuat folder baru didalam folder lib. Nah, nanti main.dart diarahkan ke file-file didalam folder baru itu.

### test

![image-20220318201128770](img\image-20220318201128770.png)

Folder ini diatur untuk testing dari kode yang kita tulis.

### assets

![image-20220318201317196](img\image-20220318201317196.png)

Folder satu ini tidak ada secara default dan hanya merupakan tambahan yang sering kita gunakan. Fungsinya untuk menyimpan `images` atau `font` tambahan. kita sarankan untuk kita tetap menjaga struktur direktori mudah ditemukan agar tidak pusing saat ingin maintenance.

## File

![image-20220318201406249](img\image-20220318201406249.png)

Ini merupakan file yang tidak masuk dalam folder yang ada diflutter, seiring berjalannya waktu. File pada flutter bisa berubah sesuai dengan kebutuhan dan pengembangan aplikasi kita.

### .gitignore

![image-20220318201614547](img\image-20220318201614547.png)

File ini berisi daftar file, extensi, dan folder yang akan diabaikan oleh Git, jika kita menggunakan Git. Dalam awal pembuatan Project Flutter baru, file ini sudah terisi dengan baik mana file atau folder yang akan diabaikan. kita tidak perlu mengubah isinya kecuali memang ada yang ingin ditambahkan.

### .metadata

![image-20220318201707233](img\image-20220318201707233.png)

File ini dikelola oleh Flutter secara otomatis digunakan untuk melacak properties dari Project Flutter. Juga untuk menilai kemampuan dan menjalankan upgrade. File ini tidak di edit secara manual.

### .packages

![image-20220318201744356](img\image-20220318201744356.png)

Isi dari file ini di generate secara otomatis oleh Flutter SDK dan digunakan untuk mendata dependencies dari Project Flutter mu. File ini tidak di edit secara manual.

### nama_project.iml

![image-20220318201825743](img\image-20220318201825743.png)

File ini akan dinamai sesuai dengan nama project dan berisi pengaturan dari Project Flutter. File ini tidak di edit secara manual.

### pubspec.yaml

![image-20220318201950546](img\image-20220318201950546.png)

File ini merupakan file konfigurasi dari Project Flutter yang akan sering kita gunakan. Hal-hal yang bisa kita konfigurasi dari file ini adalah:

- nama, deskripsi dan versi dari project
- dependencies yang digunakan
- asset seperti gambar dan font

### pubspec.lock

![image-20220318202104756](img\image-20220318202104756.png)

Pada awal kita menggunakan dependencies baru untuk sebuah package, pub akan mendownload versi paling baru dan cocok. Lalu pub akan mengunci package untuk selalu digunakan dengan membuat pubspec.lock. File ini akan mendata secara spesifik versi dari setiap dependency yang digunakan package. Saat kamu ingin upgrade dependencies ke versi paling baru, gunakan command

```cmd
pub upgrade
```

### README.md

![image-20220318202156421](img\image-20220318202156421.png)

File ini biasanya berguna sebagai guide bagi developer baru yang bergabung ke tim pengembangan Project Flutter mu. Kamu bisa tuliskan langkah awal untuk menjalankan aplikasi beserta lingkup dependencies yang perlu diperhatikan

> **PERHATIKAN!**
>
> Ada beberapa file yang tidak boleh diubah secara manual, karena itu bisa berpengaruh pada aplikasi flutter yang sedang kita buat.

## Struktur Dasar Kode Aplikasi

Saat kita membuat project baru, kode program awal yang akan kita dapatkan pada `main.dart` akan seperti ini:

```dart
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Center(child: MyHomePage(title: 'Aplikasi Flutter')),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
```

Sebenarnya kode ini sangat sederhana, yang membuat ia panjang adalah komentar-komentarnya.

Struktur kode di atas sebenarnya terdiri dari tiga bagian:

1. Bagian import;
2. Bagian main;
3. Bagian widget.

Mari kita lihat contoh kode yang sama.

```dart
import 'package:flutter/material.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatelessWidget {
  build(context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.teal,
              leading: Icon(Icons.home),
              title: Text('Aplikasi Flutter jorKlowor')
          ),
        )
    );
  }
}
```

Coba perhatikan!

#### 1. Bagian Import

```dart
import 'package:flutter/material.dart';
```

Bagian import adalah tempat kita mendeklarasikan atau mengimpor library yang dibutuhkan pada aplikasi.

#### 2. Bagian Main

```dart
void main() {
  runApp(HomePage());
}
```

Bagian main adalah fungsi utama dari aplikasi yang akan menjadi *entri point*. Fungsi ini akan dieksekusi pertama kali saat aplikasi dibuka.

#### 3. Bagian Widget

```dart
class HomePage extends StatelessWidget {
  build(context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.teal,
              leading: Icon(Icons.home),
              title: Text('Aplikasi Flutter jorKlowor')
          ),
        )
    );
  }
}
```

Bagian widget adalah tempat kita membuat widget. Aplikasi Flutter sebenarnya terdiri dari susunan widget. Widget bisa kita bilang elemen-elemen seperti Tombol, Teks, Layaout, Image, dan sebagainya.

# Struktur Widget pada Aplikasi Flutter

Setelah sebelumnya kita belajar tentang **stateless** dan stateful widget, kini kita akan coba membahas mengenai struktur widget pada aplikasi Flutter. Berikut adalah struktur widget minimal untuk aplikasi flutter.

![Struktur Widget pada Aplikasi Flutter](img\struktur-widget-flutter.png)

##### Dapat kita lihat pada gambar diatas bahwa untuk Struktur Widget pada Aplikasi Flutter, umumnya membutuhkan widget MaterialApp.

Di dalam MaterialApp widget umumnya memiliki home properti yang dapat diisi oleh widget Scaffold dan widget lainnya sebagai child. Yang paling penting untuk diingat tentang flutter adalah semuanya adalah widget dan setiap widget memiliki kegunaannya masing-masing.

### MaterialApp( )

MaterialApp adalah widget paling utama yang akan diakses pertama kali oleh fungsi **main()**. MaterialApp memiliki properti **home** yang dimana akan menjadi default route aplikasi.

```dart
void main() => MaterialApp(   
    home: Scaffold(),
);
```

Namun pada aplikasi yang sudah kita buat, kita sudah membuat **StatelessWidget** maka kita tinggal memanggil **Class** dari **StatelessWidget** kita. Caranya adalah sebagai berikut:

```dart
void main() {
  runApp(const MyApp());
}
```

Perhatikan `runApp(const MyApp());` pada `MyApp()` adalah nama **Class** yang kita panggil. jika terdapat beberapa kelas, maka kelas yang dideklarasikan saja yang akan dirender(dijalankan).

### Scaffold()

```dart
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Kamu sudah menekan tombol sebanyak: ',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
```

> **PERHATIKAN KODE DIATAS!**

Scaffold adalah widget utama yang ditampilkan di aplikasi Anda. Ini pada dasarnya adalah wadah untuk semua widget lainnya. Jika Anda hanya memiliki satu widget di aplikasi pada awalnya, itu adalah Scaffold. Scaffold juga menampung beberapa fitur dasar aplikasi seperti AppBar, Body, BottomNavigationBar, FloatingActionButton, dll.

Untuk widget-widget dibawah dari scaffold, kita juga dapat mengklasifikasikannya menjadi tiga (3) kelompok, yaitu :

1. Standalone widget
2. Single child widget
3. Multiple children widget

#### Standalone widget

Standalone widget adalah jenis widget yang tidak mengandung widget lain. Mereka memiliki fungsi tertentu dan biasanya digunakan oleh jenis widget lainnya untuk mengisi konten, atau untuk sekedar menjadi styling properties. Contoh dari Standalone widget adalah :

- AppBar
- ImageAsset
- Icon
- Text
- TextStyle

```dart
const Text(
              'Kamu sudah menekan tombol sebanyak: ',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
```

#### Single child widget

Sesuai namanya, singe child widget adalah widget yang hanya dapat memiliki **SATU** widget di dalamnya. Tipe widget ini memiliki properti yang bernama “**child**” digunakan untuk memasukan widget lain kedalam single child widget. Contoh dari single child widget yaitu :

- Center
- Container
- Expanded
- CircleAvatar
- RaisedButton
- dll

```dart
Container(
  child: Text("Belajar Flutter"),
);
```

### Multiple children widget

Widget ini dapat memiliki lebih dari satu widget di dalamnya. Tentu dalam setiap aplikasi pasti kita membutuhkan seperti kolom atau baris baik untuj layout ataupun konten. Untuk membuat hal seperti itu maka widget multiple children ini yang kamu butuhkan. Ciri dari widget ini yaitu memiliki properties yang bernama “**children**“. Contoh untuk multiple children widget yaitu :

- Row
- Column
- GridView
- Stack
- dll

```dart
Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Kamu sudah menekan tombol sebanyak: ',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
```

# Flutter SafeArea 

Dalam aplikasi yang sudah kita buat, sebenarnya kita sudah tidak perlu belajar tentang SafeArea karena sudah masuk satu paket aplikasi sederhana. Namun kita harus mengetahui teori tentang SafeArea tersebut.

SafeArea adalah widget yang berisi kotak/bantalan yang berguna untuk menutupi pada area status bar. Tujuannya agar menghindari gangguan sistem operasi. Untuk lebih jelasnya, kita langsung praktikkan menggunakan safe area, ubah file **main.dart**:

```dart
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      body: MyApp(),
    ),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.purpleAccent,
      child: SafeArea(
        child: Container(
          color: Colors.green,
          child: Center(
            child: Text("Safe Area",style: TextStyle(fontSize: 20,color: Colors.white),),
          ),
        ),
      ),
    );
  }
}
```

Tampilan Aplikasi:

![image-20220318211606729](img\image-20220318211606729.png)

Pada gambar di atas tampilan aplikasi terlihat ada dua area, yaitu area **ungu** dan area **hijau**. Untuk area ungu mencakup keseluruhan termasuk status bar, sedangkan area hijau hanya mencakup bagian badannya tanpa status bar. 

Jika diperhatikan, kita membuat dua warna stack, dimana `Container` yang berwarna ungu akan tertutup dengan `Container` yang di-nested dengan **SafeArea (berwarna hijau)**. Sehingga, seolah-olah warna ungu hanya di status bar saja padahal area ungu seluruh bagian hp, namun tertutup oleh area hijau.