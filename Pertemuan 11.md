## 3. Membuat Sign Up Page

1. Pertama-tama kita melakukan perubahan pada  **pubspec.yaml**, tambahkan `get: ^4.6.5` seperti berikut:

   ```dart
   dependencies:
     flutter:
       sdk: flutter
   
   
     # The following adds the Cupertino Icons font to your application.
     # Use with the CupertinoIcons class for iOS style icons.
     cupertino_icons: ^1.0.2
     # tambahkan package berikut
     flutter_bloc: ^8.0.1
     google_fonts: ^3.0.1
     supercharged: ^2.1.1
     flutter_spinkit: ^5.1.0
     get: ^4.6.5
   ```


2. Selanjutnya kita membuat struktur file seperti pada gambar berikut

   ![image-20220622111704088](img\image-20220622111704088.png)

3. Selanjutnya kita download gambar untuk profil pada link berikut

   https://i.pinimg.com/474x/8a/f4/7e/8af47e18b14b741f6be2ae499d23fcbe.jpg, Kemudian kita ubah namanya menjadi profile.jpg kita pindahkan ke dalam folder **asset**

   ![image-20220622112433109](img\image-20220622112433109.png)

4. Ubah file **main.dart** menjadi seperti berikut

   ```dart
   import 'package:flutter/material.dart';
   import 'package:get/get.dart';
   import 'package:supercharged/supercharged.dart';
   import 'ui/pages/pages.dart';
   
   void main() {
     runApp(MyApp());
   }
   
   class MyApp extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
       return GetMaterialApp(
         debugShowCheckedModeBanner: false,
         home: SignInPage(),
       );
     }
   }
   ```

   > `import 'package:get/get.dart';`  Kita gunakan fungsi berikut untuk berpindah dari satu halaman ke halaman lain, 
   > Fungsi ini kita gunakan pada tombol sign in dan sign up

5. Kita ubah file **pages.dart** untuk memasukkan **sign_in_page.dart** dan **sign_up_page.dart**

   ```dart
   // import 'dart:html';dart:core
   import 'dart:math';
   
   import 'package:flutter/material.dart';
   import 'package:flutter/cupertino.dart';
   import 'package:flutter_spinkit/flutter_spinkit.dart';
   import 'package:food_market/shared/shared.dart';
   import 'package:google_fonts/google_fonts.dart';
   import 'package:supercharged/supercharged.dart';
   import 'package:get/get.dart';
   
   part 'general_page.dart';
   part 'sign_in_page.dart';
   part 'sign_up_page.dart';
   ```

6. Kita ubah file **sign_in_page.dart**, Kita tambahkan fungsi untuk masuk kedalam halaman **general_page.dart**.

   ```dart
   part of 'pages.dart';
   
   class SignInPage extends StatefulWidget {
     @override
     _SignInPageState createState() => _SignInPageState();
   }
   
   class _SignInPageState extends State<SignInPage> {
     @override
     Widget build(BuildContext context) {
       TextEditingController emailController = TextEditingController();
       TextEditingController passwordController = TextEditingController();
       bool isLoading = false;
   
       return GeneralPage(
         title: 'Sign In',
         subtitle: "Find your best ever meal",
         child: Column(
           children: [
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
               child: Text(
                 "Email Address",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: emailController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your email address'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Password",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: passwordController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your password'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: isLoading
                   ? SpinKitFadingCircle(
                       size: 45,
                       color: mainColor,
                     )
                   : RaisedButton(
                       onPressed: () {
                         Get.to(GeneralPage());
                       },
                       elevation: 0,
                       shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(8)),
                       color: mainColor,
                       child: Text(
                         'Sign In',
                         style: GoogleFonts.poppins(
                             color: Colors.black, fontWeight: FontWeight.w500),
                       ),
                     ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: isLoading
                   ? SpinKitFadingCircle(
                       size: 45,
                       color: mainColor,
                     )
                   : RaisedButton(
                       onPressed: () {
                         Get.to(SignUpPage());
                       },
                       elevation: 0,
                       shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(8)),
                       color: greyColor,
                       child: Text(
                         'Create New Account',
                         style: GoogleFonts.poppins(
                             color: Colors.white, fontWeight: FontWeight.w500),
                       ),
                     ),
             )
           ],
         ),
       );
     }
   }
   ```

   pada bagian code berikut berguna untuk mengarahkan aksi jika diklik

   ```dart
   onPressed: () {
   	Get.to(GeneralPage());
   },
   ```

7. Ubah file **sign_up_page.dart**,

   ```dart
   part of 'pages.dart';
   
   class SignUpPage extends StatefulWidget {
     @override
     _SignUpPageState createState() => _SignUpPageState();
   }
   
   class _SignUpPageState extends State<SignUpPage> {
     @override
     Widget build(BuildContext context) {
       TextEditingController emailController = TextEditingController();
       TextEditingController passwordController = TextEditingController();
       TextEditingController nameController = TextEditingController();
   
       return GeneralPage(
         title: 'Sign Up',
         subtitle: "Register and eat",
         onBackButtonPressed: () {
           Get.back();
         },
         child: Column(
           children: [
             Container(
               width: 110,
               height: 110,
               margin: EdgeInsets.only(top: 26),
               padding: EdgeInsets.all(10),
               decoration: BoxDecoration(
                   image: DecorationImage(
                       image: AssetImage('assets/photo_border.png'))),
               child: Container(
                 decoration: BoxDecoration(
                     shape: BoxShape.circle,
                     image: DecorationImage(
                         image: AssetImage('assets/profil.jpg'),
                         fit: BoxFit.cover)),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Full Name",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: nameController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your full name'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Email Address",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: emailController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your email address'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Password",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: passwordController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your password'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: RaisedButton(
                 onPressed: () {},
                 elevation: 0,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(8)),
                 color: mainColor,
                 child: Text(
                   'Continue',
                   style: GoogleFonts.poppins(
                       color: Colors.black, fontWeight: FontWeight.w500),
                 ),
               ),
             ),
           ],
         ),
       );
     }
   }
   ```

8. Terakhir kita ubah file **general_page.dart** menjadi seperti berikut

   ```dart
   part of 'pages.dart';
   
   class GeneralPage extends StatelessWidget {
     final String title;
     final String subtitle;
     final Function? onBackButtonPressed;
     final Widget? child;
     final Color? backColor;
   
     GeneralPage(
         {this.title = "Title",
         this.subtitle = "subtitle",
         this.onBackButtonPressed,
         this.child,
         this.backColor});
   
     @override
     Widget build(BuildContext context) {
       return Scaffold(
           body: Stack(
         children: [
           Container(color: Colors.white),
           SafeArea(
               child: Container(
             color: backColor ?? Colors.white,
           )),
           SafeArea(
             child: ListView(
               children: [
                 Column(
                   children: [
                     Container(
                       padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                       width: double.infinity,
                       height: 100,
                       color: Colors.white,
                       child: Row(
                         children: [
                           onBackButtonPressed != null
                               ? GestureDetector(
                                   onTap: () {
                                     if (onBackButtonPressed != null) {
                                       Get.back();
                                     }
                                   },
                                   child: Container(
                                     width: 24,
                                     height: 24,
                                     margin: EdgeInsets.only(right: 26),
                                     decoration: BoxDecoration(
                                         image: DecorationImage(
                                             image: AssetImage(
                                                 'assets/back_arrow.png'))),
                                   ),
                                 )
                               : SizedBox(),
                           Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: [
                               Text(
                                 title,
                                 style: GoogleFonts.poppins(
                                     fontSize: 22, fontWeight: FontWeight.w500),
                               ),
                               Text(
                                 subtitle,
                                 style: GoogleFonts.poppins(
                                     color: "8D92A3".toColor(),
                                     fontWeight: FontWeight.w300),
                               )
                             ],
                           )
                         ],
                       ),
                     ),
                     Container(
                       height: defaultMargin,
                       width: double.infinity,
                       color: "FAFAFC".toColor(),
                     ),
                     child ?? SizedBox()
                   ],
                 ),
               ],
             ),
           ),
         ],
       ));
     }
   }
   ```

9. Oke sekarang kita coba untuk menjalankan aplikasinya

   ![image-20220622114020944](img\image-20220622114020944.png)

   ![image-20220622114103227](img\image-20220622114103227.png)

   ![image-20220622114128553](img\image-20220622114128553.png)

   

## 4. Membuat Address Page

1. Ubah **sign_up_page.dart**, Pada tombol **Continue** kita ubah aksinya menuju **address_page.dart**

   ```dart
   part of 'pages.dart';
   
   class SignUpPage extends StatefulWidget {
     @override
     _SignUpPageState createState() => _SignUpPageState();
   }
   
   class _SignUpPageState extends State<SignUpPage> {
     @override
     Widget build(BuildContext context) {
       TextEditingController emailController = TextEditingController();
       TextEditingController passwordController = TextEditingController();
       TextEditingController nameController = TextEditingController();
   
       return GeneralPage(
         title: 'Sign Up',
         subtitle: "Register and eat",
         onBackButtonPressed: () {
           Get.back();
         },
         child: Column(
           children: [
             Container(
               width: 110,
               height: 110,
               margin: EdgeInsets.only(top: 26),
               padding: EdgeInsets.all(10),
               decoration: BoxDecoration(
                   image: DecorationImage(
                       image: AssetImage('assets/photo_border.png'))),
               child: Container(
                 decoration: BoxDecoration(
                     shape: BoxShape.circle,
                     image: DecorationImage(
                         image: AssetImage('assets/profil.jpg'),
                         fit: BoxFit.cover)),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Full Name",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: nameController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your full name'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Email Address",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: emailController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your email address'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Password",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: passwordController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your password'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: RaisedButton(
                 onPressed: () {
                   Get.to(AddressPage());
                 },
                 elevation: 0,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(8)),
                 color: mainColor,
                 child: Text(
                   'Continue',
                   style: GoogleFonts.poppins(
                       color: Colors.black, fontWeight: FontWeight.w500),
                 ),
               ),
             ),
           ],
         ),
       );
     }
   }
   ```

   Perhatikan !

   ```dart
   onPressed: () {
               Get.to(AddressPage());
          },
   ```

2. Selanjutnya ubah **theme.dart** menjadi seperti berikut:

   ```dart
   part of 'shared.dart';
   
   Color mainColor = "FFC700".toColor();
   Color greyColor = "8D92A3".toColor();
   TextStyle greyFontStyle = GoogleFonts.poppins().copyWith(color: greyColor);
   TextStyle blackFontStyle1 = GoogleFonts.poppins()
       .copyWith(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500);
   TextStyle blackFontStyle2 = GoogleFonts.poppins()
       .copyWith(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500);
   TextStyle blackFontStyle3 = GoogleFonts.poppins()
       .copyWith(color: Colors.black, fontSize: 14);
   const double defaultMargin = 24;
   
   ```

3. Kemudian kita buat file **address_page.dart**, ubah seperti berikut:

   ```dart
   part of 'pages.dart';
   
   class AddressPage extends StatefulWidget {
     @override
     _AddressPageState createState() => _AddressPageState();
   }
   
   class _AddressPageState extends State<AddressPage> {
     @override
     Widget build(BuildContext context) {
       TextEditingController phoneController = TextEditingController();
       TextEditingController addressController = TextEditingController();
       TextEditingController houseNumController = TextEditingController();
   
       return GeneralPage(
         title: 'Address',
         subtitle: "Make sure it's valid",
         onBackButtonPressed: () {
           Get.back();
         },
         child: Column(
           children: [
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
               child: Text(
                 "Phone Number",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: phoneController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your phone number'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Address",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: addressController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your address'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "House Number",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: houseNumController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your house number'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "City",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: DropdownButton(
                   value: "Bandung",
                   isExpanded: true,
                   underline: SizedBox(),
                   items: [
                     DropdownMenuItem(
                       child: Text(
                         'Bandung',
                         style: blackFontStyle3,
                       ),
                       value: "Bandung",
                     ),
                     DropdownMenuItem(
                       child: Text(
                         'Jakarta',
                         style: blackFontStyle3,
                       ),
                       value: "Jakarta",
                     ),
                     DropdownMenuItem(
                       child: Text(
                         'Surabaya',
                         style: blackFontStyle3,
                       ),
                       value: "Surabaya",
                     )
                   ],
                   onChanged: (value) {}),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: RaisedButton(
                 onPressed: () {},
                 elevation: 0,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(8)),
                 color: mainColor,
                 child: Text(
                   'Sign Up Now',
                   style: GoogleFonts.poppins(
                       color: Colors.black, fontWeight: FontWeight.w500),
                 ),
               ),
             ),
           ],
         ),
       );
     }
   }
   ```

4. Terakhir kita masukkan **address_page.dart** pada **pages.dart**

   ```dart
   // import 'dart:html';dart:core
   import 'dart:math';
   
   import 'package:flutter/material.dart';
   import 'package:flutter/cupertino.dart';
   import 'package:flutter_spinkit/flutter_spinkit.dart';
   import 'package:food_market/shared/shared.dart';
   import 'package:google_fonts/google_fonts.dart';
   import 'package:supercharged/supercharged.dart';
   import 'package:get/get.dart';
   
   part 'general_page.dart';
   part 'sign_in_page.dart';
   part 'sign_up_page.dart';
   part 'address_page.dart';
   ```

5. Jalankan kode!

   ![image-20220622141441230](img\image-20220622141441230.png)