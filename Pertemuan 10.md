# Membuat Foodmartket Apps

> **Pastikan flutter telah terinstall tanpa error!**

Untuk memastikan apakah plugin flutter sudah working terpasang tanpa error:

1. Tekan shortcut `ctrl + shift + p`

2. Ketik `"Flutter"`

3. Jika kita mendapatkan hasil seperti ini:

   ![image-20220311095625735](img/image-20220311095625735.png)

4. Pilih **Application**

5. Selanjutnya kita masukkan nama projek kita yaitu **"food_market"** Kemudian tekan Enter

6. Tunggu sampai projek baru selesai dibuat.

   ![image-20220604085731981](img\image-20220604085731981.png)

Link di bawah ini merupakan bahan yang yang kita perlukan untuk membuat food market:

1. Bahan Project

    https://drive.google.com/drive/folders/1xnMYHLKGt7b9mxztBwmsdprnr3gMe84f?usp=sharing

2. Desain Project 

   https://www.figma.com/file/6E0dolZ0Dy5CHVi4qkwUqq/FoodMarket?node-id=0%3A1

Berikut adalah gambaran dari figma

![image-20220604090343318](img\image-20220604090343318.png)

# Membuat General Page

Pertama kita harus membuat struktur folder seperti berikut

![image-20220604090844153](img\image-20220604090844153.png)

Kemudian kita buka file **pubspec.yaml** Selanjutnya kita ubah pada blok kode **dependencies** menjadi seperti berikut

```dart
dependencies:
  flutter:
    sdk: flutter


  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^1.0.2
  # tambahkan package berikut
  flutter_bloc: ^8.0.1
  google_fonts: ^3.0.1
  supercharged: ^2.1.1
```

## 1. Membuat General Page

Selanjutnya kita buat **general_page.dart** dan **pages.dart** pada folder **ui/pages**

![image-20220604091952303](img\image-20220604091952303.png)

1. Ubah **main.dart** seperti berikut 

   ```dart
   import 'package:flutter/material.dart';
   import 'ui/pages/pages.dart';
   
   void main() {
     runApp(MyApp());
   }
   class MyApp extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
       return MaterialApp(
         debugShowCheckedModeBanner: false,
         home: GeneralPage(),
       );
     }
   }
   ```

   Kita tambahkan **StatelessWidget**

2. Ubah **general_page.dart** seperti berikut

   ```dart
   part of 'pages.dart';
   
   class GeneralPage extends StatelessWidget {
   
     @override
     Widget build(BuildContext context) {
       return Scaffold(
         body: Container(),
       );
     }
   }
   ```

3. Ubah **pages.dart** seperti berikut

   ```dart
   import 'dart:html';
   
   import 'package:flutter/material.dart';
   
   part 'general_page.dart';
   ```

4. Kemudian kita jalankan dengan menekan tombol **F5**

   ![image-20220604093040016](img\image-20220604093040016.png)

   > Jangan lupa ketika ingin menjalankan program kita harus membuka file **main.dart**

Untuk tampilan awal masih putih seperti ini selanjutnya kita buat tampilan untuk di General Pagenya

1. Ubah struktur folder menjadi seperti berikut

   ![image-20220604102437631](img\image-20220604102437631.png)

2. Pertama kita ubah **main.dart**

   ```dart
   import 'package:flutter/material.dart';
   import 'ui/pages/pages.dart';
   import 'package:supercharged/supercharged.dart';
   
   void main() {
     runApp(MyApp());
   }
   class MyApp extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
       return MaterialApp(
         debugShowCheckedModeBanner: false,
         home: GeneralPage(onBackButtonPressed: (){} ),
       );
     }
   }
   ```

   Pada `GeneralPage()` kita tambahkan fungsi **onBackButtonPressed** 

3. Selanjutnya kita edit **general_page.dart** menjadi seperti berikut 

   ```dart
   part of 'pages.dart';
   
   class GeneralPage extends StatelessWidget {
     final String title;
     final String subtitle;
     final Function? onBackButtonPressed;
     final Widget? child;
     final Color? backColor;
   
     GeneralPage(
         {this.title = "Title",
         this.subtitle = "subtitle",
         this.onBackButtonPressed,
         this.child,
         this.backColor});
   
     @override
     Widget build(BuildContext context) {
       return Scaffold(
         body: Stack(
           children: [
             Container(
               color: Colors.white,
             ),
             SafeArea(
                 child: Container(
               color: backColor ?? "FAFAFC".toColor(),
             )),
             SafeArea(
               child: ListView(
                 children: [
                   Column(
                     children: [
                       Container(
                         margin: EdgeInsets.only(bottom: defaultMargin),
                         padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                         width: double.infinity,
                         height: 100,
                         color: Colors.white,
                         child: Row(
                           children: [
                             onBackButtonPressed != null
                                 ? Container(
                                     width: 24,
                                     height: 24,
                                     child: Text('<'),
                                   )
                                 : SizedBox(),
                             Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: [
                                 Text(title,
                                     style: GoogleFonts.poppins(
                                       fontSize: 22,
                                       fontWeight: FontWeight.w500,
                                     )),
                                 Text(subtitle,
                                     style: GoogleFonts.poppins(
                                       color: "8D92A3".toColor(),
                                       fontWeight: FontWeight.w300,
                                     )),
                               ],
                             )
                           ],
                         ),
                       ),
                       child ?? SizedBox(),
                     ],
                   ),
                 ],
               ),
             ),
           ],
         ),
       );
     }
   }
   ```

   Disini kita menambahkan title, subtitle dan lain sebagainya 

4. Pastikan **pages.dart** sudah berikut 

   ```dart
   import 'dart:html';
   
   import 'package:flutter/material.dart';
   import 'package:flutter/cupertino.dart';
   import 'package:food_market/shared/shared.dart';
   import 'package:google_fonts/google_fonts.dart';
   import 'package:supercharged/supercharged.dart';
   
   part 'general_page.dart';
   ```

5. Pastikan **shared.dart** sudah berikut

   ```dart
   import 'package:flutter/material.dart';
   import 'package:google_fonts/google_fonts.dart';
   import 'package:supercharged/supercharged.dart';
   part 'theme.dart';
   ```

6. Yang terakhir kita tambahkan **theme.dart** untuk menambahkan variabel-variabel yang biasa kita gunakan, seperti tema warna dan juga tema font.

   ```dart
   part of 'shared.dart';
   
   Color mainColor = "FFC700".toColor();
   Color greyColor = "8D92A3".toColor();
   TextStyle greyFontStyle = GoogleFonts.poppins().copyWith(color: greyColor);
   const double defaultMargin = 24;
   ```

7. Selanjutnya kita tambahkan folder **assets** dan isi folder tersebut dengan aset yang sudah kita download pada link diatas.

   foldernya assets (**Bahan FoodMarket Apps\Part 1A. Flutter UI\Flutter Assets**)

   ![image-20220604103335450](img\image-20220604103335450.png)

8. Selanjutnya kita ubah **general_page.dart** ,Kita ubah tanda `onBackButtonPressed` dengan gambar dari aset.

   ```dart
   part of 'pages.dart';
   
   class GeneralPage extends StatelessWidget {
     final String title;
     final String subtitle;
     final Function? onBackButtonPressed;
     final Widget? child;
     final Color? backColor;
   
     GeneralPage(
         {this.title = "Title",
         this.subtitle = "subtitle",
         this.onBackButtonPressed,
         this.child,
         this.backColor});
   
     @override
     Widget build(BuildContext context) {
       return Scaffold(
         body: Stack(
           children: [
             Container(
               color: Colors.white,
             ),
             SafeArea(
                 child: Container(
               color: backColor ?? "FAFAFC".toColor(),
             )),
             SafeArea(
               child: ListView(
                 children: [
                   Column(
                     children: [
                       Container(
                         margin: EdgeInsets.only(bottom: defaultMargin),
                         padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                         width: double.infinity,
                         height: 100,
                         color: Colors.white,
                         child: Row(
                           children: [
                             onBackButtonPressed != null
                                 ? Container(
                                     width: 24,
                                     height: 24,
                                     margin: EdgeInsets.only(right: 26),
                                     decoration: BoxDecoration(
                                         image: DecorationImage(
                                             image: AssetImage(
                                                 'assets/back_arrow.png'))),
                                   )
                                 : SizedBox(),
                             Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: [
                                 Text(title,
                                     style: GoogleFonts.poppins(
                                       fontSize: 22,
                                       fontWeight: FontWeight.w500,
                                     )),
                                 Text(subtitle,
                                     style: GoogleFonts.poppins(
                                       color: "8D92A3".toColor(),
                                       fontWeight: FontWeight.w300,
                                     )),
                               ],
                             )
                           ],
                         ),
                       ),
                       child ?? SizedBox(),
                     ],
                   ),
                 ],
               ),
             ),
           ],
         ),
       );
     }
   }
   ```

9. Yang terakhir kita beri tulisan body  pada **main.dart**

   ```dart
   import 'package:flutter/material.dart';
   import 'ui/pages/pages.dart';
   import 'package:supercharged/supercharged.dart';
   
   void main() {
     runApp(MyApp());
   }
   
   class MyApp extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
       return MaterialApp(
         debugShowCheckedModeBanner: false,
         home: GeneralPage(
           onBackButtonPressed: () {},
           child: Text("body"),
         ),
       );
     }
   }
   ```

10. Berikut adalah hasil dari general page yang sudah kita buat

    ![image-20220604104448347](img\image-20220604104448347.png)

## 2. Membuat Sign In Page

1. Pertama-tama kita melakukan perubahan pada ada warna default dari body, kita Ubah menjadi warna putih. Edit **general_page.dart**

   ```dart
   part of 'pages.dart';
   
   class GeneralPage extends StatelessWidget {
     final String title;
     final String subtitle;
     final Function? onBackButtonPressed;
     final Widget? child;
     final Color? backColor;
   
     GeneralPage(
         {this.title = "Title",
         this.subtitle = "subtitle",
         this.onBackButtonPressed,
         this.child,
         this.backColor});
   
     @override
     Widget build(BuildContext context) {
       return Scaffold(
         body: Stack(
           children: [
             Container(
               color: Colors.white,
             ),
             SafeArea(
                 child: Container(
               color: backColor ?? Colors.white,
             )),
             SafeArea(
               child: ListView(
                 children: [
                   Column(
                     children: [
                       Container(
                         padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                         width: double.infinity,
                         height: 100,
                         color: Colors.white,
                         child: Row(
                           children: [
                             onBackButtonPressed != null
                                 ? Container(
                                     width: 24,
                                     height: 24,
                                     margin: EdgeInsets.only(right: 26),
                                     decoration: BoxDecoration(
                                         image: DecorationImage(
                                             image: AssetImage(
                                                 'assets/back_arrow.png'))),
                                   )
                                 : SizedBox(),
                             Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: [
                                 Text(title,
                                     style: GoogleFonts.poppins(
                                       fontSize: 22,
                                       fontWeight: FontWeight.w500,
                                     )),
                                 Text(subtitle,
                                     style: GoogleFonts.poppins(
                                       color: "8D92A3".toColor(),
                                       fontWeight: FontWeight.w300,
                                     )),
                               ],
                             )
                           ],
                         ),
                       ),
                       Container(
                         height: defaultMargin,
                         width: double.infinity,
                         color: "FAFAFC".toColor(),
                       ),
                       child ?? SizedBox(),
                     ],
                   ),
                 ],
               ),
             ),
           ],
         ),
       );
     }
   }
   ```

​		![image-20220604191328130](img\image-20220604191328130.png)

2. Selanjutnya kita buat tema warna untuk font yang akan kita gunakan 
   Buka **theme.dart**

   ```dart
   part of 'shared.dart';
   
   Color mainColor = "FFC700".toColor();
   Color greyColor = "8D92A3".toColor();
   TextStyle greyFontStyle = GoogleFonts.poppins().copyWith(color: greyColor);
   TextStyle blackFontStyle1 = GoogleFonts.poppins()
       .copyWith(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500);
   TextStyle blackFontStyle2 = GoogleFonts.poppins()
       .copyWith(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500);
   TextStyle blackFontStyle = GoogleFonts.poppins()
       .copyWith(color: Colors.black, fontSize: 14);
   const double defaultMargin = 24;
   ```

3. Selanjutnya kita tambahkan kode seperti berikut `flutter_spinkit: ^5.1.0` untuk menggunakan flutter spinkit pada **pubspec.yaml**

   ```dart
   dependencies:
     flutter:
       sdk: flutter
   
   
     # The following adds the Cupertino Icons font to your application.
     # Use with the CupertinoIcons class for iOS style icons.
     cupertino_icons: ^1.0.2
     # tambahkan package berikut
     flutter_bloc: ^8.0.1
     google_fonts: ^3.0.1
     supercharged: ^2.1.1
     flutter_spinkit: ^5.1.0
   ```

4. Selanjutnya kita buat **sign_in_page.dart** 

   ```dart
   part of 'pages.dart';
   
   class SignInPage extends StatefulWidget {
     @override
     _SignInPageState createState() => _SignInPageState();
   }
   
   class _SignInPageState extends State<SignInPage> {
     @override
     Widget build(BuildContext context) {
       TextEditingController emailController = TextEditingController();
       TextEditingController passwordController = TextEditingController();
       bool isLoading = false;
   
       return GeneralPage(
         title: 'Sign In',
         subtitle: "Find your best ever meal",
         child: Column(
           children: [
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
               child: Text(
                 "Email Address",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: emailController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your email address'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
               child: Text(
                 "Password",
                 style: blackFontStyle2,
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.symmetric(horizontal: defaultMargin),
               padding: EdgeInsets.symmetric(horizontal: 10),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(8),
                   border: Border.all(color: Colors.black)),
               child: TextField(
                 controller: passwordController,
                 decoration: InputDecoration(
                     border: InputBorder.none,
                     hintStyle: greyFontStyle,
                     hintText: 'Type your password'),
               ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: isLoading
                   ? SpinKitFadingCircle(
                       size: 45,
                       color: mainColor,
                     )
                   : RaisedButton(
                       onPressed: () {},
                       elevation: 0,
                       shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(8)),
                       color: mainColor,
                       child: Text(
                         'Sign In',
                         style: GoogleFonts.poppins(
                             color: Colors.black, fontWeight: FontWeight.w500),
                       ),
                     ),
             ),
             Container(
               width: double.infinity,
               margin: EdgeInsets.only(top: 24),
               height: 45,
               padding: EdgeInsets.symmetric(horizontal: defaultMargin),
               child: isLoading
                   ? SpinKitFadingCircle(
                       size: 45,
                       color: mainColor,
                     )
                   : RaisedButton(
                       onPressed: () {},
                       elevation: 0,
                       shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(8)),
                       color: greyColor,
                       child: Text(
                         'Create New Account',
                         style: GoogleFonts.poppins(
                             color: Colors.white, fontWeight: FontWeight.w500),
                       ),
                     ),
             )
           ],
         ),
       );
     }
   }
   ```

5. Pastikan **sign_in_page.dart** sudah masuk dalam **page.dart**

   ```dart
   import 'dart:html';
   
   import 'package:flutter/material.dart';
   import 'package:flutter/cupertino.dart';
   import 'package:flutter_spinkit/flutter_spinkit.dart';
   import 'package:food_market/shared/shared.dart';
   import 'package:google_fonts/google_fonts.dart';
   import 'package:supercharged/supercharged.dart';
   
   part 'general_page.dart';
   part 'sign_in_page.dart';
   ```

6. Kemudian pada `home` kita Ubah menjadi **SignInPage()**

   ```dart
   import 'package:flutter/material.dart';
   import 'ui/pages/pages.dart';
   import 'package:supercharged/supercharged.dart';
   
   void main() {
     runApp(MyApp());
   }
   
   class MyApp extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
       return MaterialApp(
         debugShowCheckedModeBanner: false,
         home: SignInPage(),
       );
     }
   }
   ```

   ![image-20220604193852675](img\image-20220604193852675.png)

