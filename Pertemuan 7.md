# **2. GoPay Menu**

Pada materi sebelumnya kita sudah membagi halaman landing page menjadi 4 bagian, yaitu **App Bar, Gopay Menu, Gojek Menu, Go Food Menu** dan bagian lain yaitu list untuk promo yang berada pada bagian bawah **Go Food Menu**.

![img](img\layout.png)

Pertama-tama copy terlebih dahulu asset untuk menu transfer, scan qr, isi saldo dan lainnya, asset dapat diunduh [disini](https://drive.google.com/open?id=189WHYsR703f1uwRRImIuwy0tvYNpxKHH)

Pada directory **assets** buat directory baru bernama **icons** kemudian copy icon-icon tersebut ke directory **icons.** 

![image-20220420091933679](img\image-20220420091933679.png)

Selanjutnya jangan lupa untuk mendaftarkan asset icon tersebut ke **pubspec.yaml** sesuai dengan nama filenya

```dart
name: gojekapp
description: A new Flutter project.

# The following line prevents the package from being accidentally published to
# pub.dev using `flutter pub publish`. This is preferred for private packages.
publish_to: 'none' # Remove this line if you wish to publish to pub.dev

# The following defines the version and build number for your application.
# A version number is three numbers separated by dots, like 1.2.43
# followed by an optional build number separated by a +.
# Both the version and the builder number may be overridden in flutter
# build by specifying --build-name and --build-number, respectively.
# In Android, build-name is used as versionName while build-number used as versionCode.
# Read more about Android versioning at https://developer.android.com/studio/publish/versioning
# In iOS, build-name is used as CFBundleShortVersionString while build-number used as CFBundleVersion.
# Read more about iOS versioning at
# https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CoreFoundationKeys.html
version: 1.0.0+1

environment:
  sdk: ">=2.16.1 <3.0.0"

# Dependencies specify other packages that your package needs in order to work.
# To automatically upgrade your package dependencies to the latest versions
# consider running `flutter pub upgrade --major-versions`. Alternatively,
# dependencies can be manually updated by changing the version numbers below to
# the latest version available on pub.dev. To see which dependencies have newer
# versions available, run `flutter pub outdated`.
dependencies:
  flutter:
    sdk: flutter


  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^1.0.2

dev_dependencies:
  flutter_test:
    sdk: flutter

  # The "flutter_lints" package below contains a set of recommended lints to
  # encourage good coding practices. The lint set provided by the package is
  # activated in the `analysis_options.yaml` file located at the root of your
  # package. See that file for information about deactivating specific lint
  # rules and activating additional ones.
  flutter_lints: ^1.0.0

# For information on the generic Dart part of this file, see the
# following page: https://dart.dev/tools/pub/pubspec

# The following section is specific to Flutter.
flutter:

  # The following line ensures that the Material Icons font is
  # included with your application, so that you can use the icons in
  # the material Icons class.
  uses-material-design: true

  # To add assets to your application, add an assets section, like this:
  assets:
  - assets/img/img_gojek_logo.png
  - assets/icons/icon_menu.png 
  - assets/icons/icon_saldo.png 
  - assets/icons/icon_scan.png 
  - assets/icons/icon_transfer.png 
  #   - images/a_dot_ham.jpeg

  # An image asset can refer to one or more resolution-specific "variants", see
  # https://flutter.dev/assets-and-images/#resolution-aware.

  # For details regarding adding assets from package dependencies, see
  # https://flutter.dev/assets-and-images/#from-packages

  # To add custom fonts to your application, add a fonts section here,
  # in this "flutter" section. Each entry in this list should have a
  # "family" key with the font family name, and a "fonts" key with a
  # list giving the asset and other descriptors for the font. For
  # example:
  fonts:
    - family: NeoSans
      fonts:
        - asset: assets/fonts/Neo Sans Std Regular.otf
    - family: NeoSansBold
      fonts:
        - asset: assets/fonts/Neo Sans Std Bold.otf
    # - family: Trajan Pro
    #   fonts:
    #     - asset: fonts/TrajanPro.ttf
    #     - asset: fonts/TrajanPro_Bold.ttf
    #       weight: 700
  #
  # For details regarding fonts from package dependencies,
  # see https://flutter.dev/custom-fonts/#from-packages
```

Kemudian kembali ke halaman **beranda_view.dart** buat sebuah methode **_buildGopayMenu()** untuk memisahkan tampilan untuk gopay menu sehingga lebih mudah dicari.

```dart
Widget _buildGopayMenu() {
  return new Container(
      height: 120.0,
      decoration: new BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
          ),
          borderRadius: new BorderRadius.all(new Radius.circular(3.0))),
      child: new Column(
        children: <Widget>[
          new Container(
            padding: EdgeInsets.all(12.0),
            decoration: new BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
                ),
                borderRadius: new BorderRadius.only(
                    topLeft: new Radius.circular(3.0),
                    topRight: new Radius.circular(3.0))),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  "GOPAY",
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontFamily: "NeoSansBold"),
                ),
                new Container(
                  child: new Text(
                    "Rp. 120.000",
                    style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                        fontFamily: "NeoSansBold"),
                  ),
                )
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_transfer.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Transfer",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_scan.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Scan QR",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_saldo.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Isi Saldo",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_menu.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Lainnya",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ));
}
```

Pada bagian ini kita akan belajar bagaimana membuat sebuah gradien pada **container**, untuk membuat sebuah gradien dibutuhkan sebuah **container**, kemudian pada container tersebut ditambahkan property **gradien** seperti berikut :

```dart
decoration: new BoxDecoration(            
gradient: LinearGradient(             
begin: Alignment.topCenter,           
end: Alignment.bottomCenter,              
colors: [const Color(0xff3164bd), const Color(0xff295cb5)],           ),
```

bagian **begin** menyatakan darimana gradien dimulai, 

pada **case** kita adalah **topCenter** dan berakhir (end) pada bottomCenter. 

Selain begin dan end juga terdapat center, atau bagian tengah gradien jika ingin ditambahkan.

Kemudian method **_buildGopayMenu()** panggil pada method **build()** pada bagian body. Jadi file **beranda_view.dart** menjadi seperti ini:

```dart
import 'package:flutter/material.dart';
import 'package:gojekapp/beranda/beranda_gojek_appbar.dart';
import 'package:gojekapp/constant.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => new _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
        appBar: new GojekAppBar(),
        backgroundColor: GojekPalette.grey,
        body: new Container(
          child: new ListView(
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              new Container(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                  color: Colors.white,
                  child: new Column(
                    children: <Widget>[
                      _buildGopayMenu(),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buildGopayMenu() {
  return new Container(
      height: 120.0,
      decoration: new BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
          ),
          borderRadius: new BorderRadius.all(new Radius.circular(3.0))),
      child: new Column(
        children: <Widget>[
          new Container(
            padding: EdgeInsets.all(12.0),
            decoration: new BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
                ),
                borderRadius: new BorderRadius.only(
                    topLeft: new Radius.circular(3.0),
                    topRight: new Radius.circular(3.0))),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  "GOPAY",
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontFamily: "NeoSansBold"),
                ),
                new Container(
                  child: new Text(
                    "Rp. 120.000",
                    style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                        fontFamily: "NeoSansBold"),
                  ),
                )
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_transfer.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Transfer",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_scan.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Scan QR",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_saldo.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Isi Saldo",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image.asset(
                      "assets/icons/icon_menu.png",
                      width: 32.0,
                      height: 32.0,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    new Text(
                      "Lainnya",
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ));
}
```

Kemudian kita run, maka hasilnya akan seperti ini.

![image-20220420093058387](img\image-20220420093058387.png)

> Catatan : 
>
> Apabila terdapat error asset not found ketika aplikasi dijalankan, pastikan path asset yang sudah didaftarkan di **pubspec.yaml** sudah benar. 
>
> Jika sudah dipastikan benar tapi masih error maka uninstall terlebih dahulu aplikasi kemudian jalankan lagi.

**3. Membuat GoJek Menu**

Pada bagian Gojek Menu kita tidak akan menambahkan asset baru pada project yang kita buat, kita akan memanfaatkan icon-icon generic material design yang sudah disediakan oleh flutter.

Pertama-tama buat method **_buildGojekServicesMenu()** seperti pada **Gopay Menu**, ini dimaksudkan untuk memudahkan pencarian bagian view yang kita buat.

```dart
Widget _buildGojekServicesMenu() {
  return new SizedBox(
      width: double.infinity,
      height: 220.0,
      child: new Container(
          margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
          child: GridView.builder(
              physics: ClampingScrollPhysics(),
              itemCount: 8,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4),
              itemBuilder: (context, position) {
                return new Text("Gojek Menu");
              })));
}
```

Pada bagian ini kita akan membuat **GridView** dengan menggunakan **GridView builder**, **itemCount** menandakan berapa banyak jumlah yang akan ditampilkan oleh **GridView**.

Kemudian **gridDelegate** menggunakan **SliverGridDelegateWithFixedCrossAxisCount** dengan **crossAxisCount 4** yang menandakan grid tersebut mempunyai empat column.

Kemudian tambahkan method **_buildGojekServicesMenu()** ke method **build()** tepat dibawah _**buildGopayMenu()** yang sudah kita buat sebelumnya(**beranda_view.dart**), kemudian jalankan.

```dart
class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
        appBar: new GojekAppBar(),
        backgroundColor: GojekPalette.grey,
        body: new Container(
          child: new ListView(
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              new Container(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                  color: Colors.white,
                  child: new Column(
                    children: <Widget>[
                      _buildGopayMenu(),
                      // method baru
                      _buildGojekServicesMenu()
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
```

![image-20220420093820465](img\image-20220420093820465.png)

Jika tampilan yang dihasilkan seperti diatas berarti kita sudah berhasil membuat menu dengan gridview. Karena pada bagian itemBuilder kita hanya memberikan balikan sebuah text “**Gojek Menu**” dimana text tersebut akan kita ubah menjadi grid yang dinamis dengan tampilan icon dan text sesuai dengan tampilan pada GOJEK.