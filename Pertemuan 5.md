# Membangun Landing Page Aplikasi GO-JEK dengan Flutter SDK

## Instalasi Flutter Baru dengan Vs Code

Pastikan VSCode sudah terinstal pada komputer kita. Dengan menggunakan plugin Flutter official dan Dart, teks editor vscode bisa kita sulap untuk membuat dan membangun project flutter.

## 1. Buka VSCode

Langkah yang pertama adalah buka aplikasi VSCode.

![image-20220311095304124](img\image-20220311095304124.png)

## 2. Buka Menu Extensions

Anda bisa membuka menu extensions dengan menekan tombol `Ctrl + Shift + X` atau dengan langsung mengklik tombol extensions di panel menu sebelah kiri.

## 3. Install Flutter & Dart Plugin

Setelah itu ketik “Flutter” pada kotak pencarian.

![image-20220311095413185](img\image-20220311095413185.png)

Klik hasil yang paling atas.

Klik tombol **install**.

Tunggu sampai proses selesai.

Ketika kita memasang plugin flutter, harusnya plugin dart juga akan terinstall secara otomatis. Tapi untuk memastikan lebih jauh, anda bisa mencari plugin Dart dan lihat apakah sudah terinstall atau tidak.

![image-20220311095541355](img\image-20220311095541355.png)

## 4. Pastikan flutter telah terinstall tanpa error

Untuk memastikan apakah plugin flutter sudah working terpasang tanpa error:

1. Tekan shortcut `ctrl + shift + p`

2. Ketik `"Flutter"`

3. Jika kita mendapatkan hasil seperti ini:

   ![image-20220311095625735](img\image-20220311095625735.png)

4. Masukkan **"GOJEKAPP"** Kemudian tekan Enter

5. Tunggu sampai projek baru selesai dibuat.

   ![image-20220407201737878](img\image-20220407201737878.png)

Untuk memudahkan belajar membangun UI pada Flutter, kita menggunakan aplikasi GO-JEK sebagai contoh, sehingga dari contoh aplikasi tersebut kita bisa belajar bagaimana membangun List, Grid, dan tata letak komponen view pada Flutter, tulisan ini akan kita bagi per komponen view pada aplikasi GO-JEK.

![img](img\gojekgif)

Cara membuatnya dapat mengikuti panduan dari https://flutter.io/get-started/install/ sehingga ketika kita run aplikasinya akan seperti gambar dibawah ini.

![image-20220407204310054](img\image-20220407204310054.png)

# **Splash Screen**

![img](img\6btKklBdkeEPwqEDZcoqEA.png)

**1. Membuat Halaman Baru**

Pertama-tama buat folder baru pada directory **lib** untuk memisahkan halaman screen dengan halaman main. Buat directory dengan nama **launcher.** Kemudian pada folder tersebut buat sebuah file dengan nama **launcher_view.dart** untuk membuat halaman splash tersebut. 

![image-20220407204541948](img\image-20220407204541948.png)

Kemudian buat sebuah class baru bernama **LauncherPage** pada file **launcher_view.dart** seperti kode berikut :

```dart
import 'package:flutter/material.dart';

class LauncherPage extends StatefulWidget {
  @override
  _LauncherPageState createState() => new _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold();
  }
}
```

Kembali ke halaman main, hapus class **MyHomePage** pada file **main.dart**, kemudian pada atribut **home** ganti dengan new **LauncherPage()** seperti kode dibawah ini.

```dart
import 'launcher/launcher_view.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gojek',
      theme: new ThemeData(),
      home: new LauncherPage(),
    );
  }
}
```

Maka setelah kita run yang pertama kali muncul adalah halaman **LauncherPage.**

**2. Menambahkan Asset**

Section selanjutnya adalah menambahkan asset pada project yang kita buat, baik berupa font, gambar, sound dan lain sebagainya. Pada project yang kita buat pertama-tama kita akan menambahkan asset berupa logo gojek seperti yang terlihat pada splash screen.

![image-20220407205449654](img\image-20220407205449654.png)

Pertama-tama buat folder **assets** yang sejajar dengan folder **lib** kemudian buat folder **img** didalamnya. Selanjutnya tambahkan file gambar yang akan kita masukan ke dalam folder **img** tersebut. beri nama **img_gojek_logo.png** (Logo GOJEK : https://goo.gl/uqPsYC)**.** Setelah itu tambahkan directory assets tersebut agar project kita mengenali file asset yang sudah kita masukan tadi.

Caranya dengan membuka menambahkan baris aset berikut pada file **pubspec.yaml**

```dart
name: gojekapp
description: A new Flutter project.

# The following line prevents the package from being accidentally published to
# pub.dev using `flutter pub publish`. This is preferred for private packages.
publish_to: 'none' # Remove this line if you wish to publish to pub.dev

# The following defines the version and build number for your application.
# A version number is three numbers separated by dots, like 1.2.43
# followed by an optional build number separated by a +.
# Both the version and the builder number may be overridden in flutter
# build by specifying --build-name and --build-number, respectively.
# In Android, build-name is used as versionName while build-number used as versionCode.
# Read more about Android versioning at https://developer.android.com/studio/publish/versioning
# In iOS, build-name is used as CFBundleShortVersionString while build-number used as CFBundleVersion.
# Read more about iOS versioning at
# https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CoreFoundationKeys.html
version: 1.0.0+1

environment:
  sdk: ">=2.16.1 <3.0.0"

# Dependencies specify other packages that your package needs in order to work.
# To automatically upgrade your package dependencies to the latest versions
# consider running `flutter pub upgrade --major-versions`. Alternatively,
# dependencies can be manually updated by changing the version numbers below to
# the latest version available on pub.dev. To see which dependencies have newer
# versions available, run `flutter pub outdated`.
dependencies:
  flutter:
    sdk: flutter


  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^1.0.2

dev_dependencies:
  flutter_test:
    sdk: flutter

  # The "flutter_lints" package below contains a set of recommended lints to
  # encourage good coding practices. The lint set provided by the package is
  # activated in the `analysis_options.yaml` file located at the root of your
  # package. See that file for information about deactivating specific lint
  # rules and activating additional ones.
  flutter_lints: ^1.0.0

# For information on the generic Dart part of this file, see the
# following page: https://dart.dev/tools/pub/pubspec

# The following section is specific to Flutter.
flutter:

  # The following line ensures that the Material Icons font is
  # included with your application, so that you can use the icons in
  # the material Icons class.
  uses-material-design: true

  # To add assets to your application, add an assets section, like this:
  assets:
  - assets/img/img_gojek_logo.png
  #   - images/a_dot_ham.jpeg

  # An image asset can refer to one or more resolution-specific "variants", see
  # https://flutter.dev/assets-and-images/#resolution-aware.

  # For details regarding adding assets from package dependencies, see
  # https://flutter.dev/assets-and-images/#from-packages

  # To add custom fonts to your application, add a fonts section here,
  # in this "flutter" section. Each entry in this list should have a
  # "family" key with the font family name, and a "fonts" key with a
  # list giving the asset and other descriptors for the font. For
  # example:
  # fonts:
  #   - family: Schyler
  #     fonts:
  #       - asset: fonts/Schyler-Regular.ttf
  #       - asset: fonts/Schyler-Italic.ttf
  #         style: italic
  #   - family: Trajan Pro
  #     fonts:
  #       - asset: fonts/TrajanPro.ttf
  #       - asset: fonts/TrajanPro_Bold.ttf
  #         weight: 700
  #
  # For details regarding fonts from package dependencies,
  # see https://flutter.dev/custom-fonts/#from-packages
```

Dengan mandaftarkan path asset pada file **pubspec.yaml** kita sudah bisa menggunakan asset tersebut pada project yang kita buat. Setelah asset sudah ditambahkan maka modifikasi class **_LauncherPageState** pada file **launcher_page.dart** menjadi seperti dibawah ini :

```dart
import 'package:flutter/material.dart';

class LauncherPage extends StatefulWidget {
  @override
  _LauncherPageState createState() => new _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.asset(
          "assets/img/img_gojek_logo.png",
          height: 100.0,
          width: 200.0,
        ),
      ),
    );
  }
}
```

![image-20220407210137027](img\image-20220407210137027.png)

**3. Membuat Timer dan Berpindah Halaman**

Section selanjutnya adalah membuat timer. Ketika halaman splash terbuka maka akan diam beberapa saat sebelum berpindah ke landing page. Sebelum membuat code untuk timer sebaiknya kita siapkan dulu file untuk halaman **LandingPage,** buat folder baru pada directory **lib** dengan nama **landingpage,** kemudian buat file pada directory tersebut dengan nama **landingpage_view.dart.** 

![image-20220407210531469](img\image-20220407210531469.png)

Setelah itu buat dua buah class bernama **LandingPage** dan _**LandingPageState** seperti pertama kali kita membuat class **LauncherPage** dan **LauncherPageState**.

```dart
import 'package:flutter/material.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPage createState() => new _LandingPage();
}

class _LandingPage extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: Text(
          "Landing Page",
        ),
      ),
    );
  }
}

```

Kemudian pada class _**LauncherPageState** modifikasi seperti dibawah ini :

```dart
import 'package:flutter/material.dart';
import 'dart:async';
import '../landingpage/landingpage_view.dart';

class LauncherPage extends StatefulWidget {
  @override
  _LauncherPageState createState() => new _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {
  @override
  void initState() {
    super.initState();
    startLaunching();
  }

  startLaunching() async {
    var duration = const Duration(seconds: 3);
    return new Timer(duration, () {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) {
        return new LandingPage();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.asset(
          "assets/img/img_gojek_logo.png",
          height: 100.0,
          width: 200.0,
        ),
      ),
    );
  }
}

```

Pertama-tama override initState (kurang lebih seperti onCreate() pada Android dengan Java), kemudian buat class asynchronous bernama **startTimer** yang dipanggil pada **initState**, kemudian pada timer terdapat Function yang akan dijalankan ketika timer selesai, maka kita akan menambahkan fungsi untuk berpindah ke halaman **LandingPage** yaitu dengan

```dart
Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) { 
     return new LandingPage();      
}));
```

Setelah 3 detik halaman akan berpindah ke **LandingPage**

![image-20220407212011661](img\image-20220407212011661.png)

Ada beberapa cara untuk berpindah halaman, seperti dengan **push**, **pushReplacement**, **pushReplaceNamed** dan sebagainya, tapi saya cenderung lebih sering menggunakan **push** dan **pushReplacement.** 

Apa perbedaan anatara **push** dan **pushReplacement**? jika **pushReplacement** maka activity sebelumnya tidak akan ditampilkan lagi ketika kita menekan tombol back, alias di **finish()** ketika kita berpindah halaman dengan Intent pada java, sedangkan **push** masih akan menyimpan halaman sebelumnya sebagai stack, sehingga ketika kita berpindah halaman dengan **push** maka pada appbar yang kita buat akan otomatis muncul tombol back, sedangkan juka **pushReplacemant** tidak.