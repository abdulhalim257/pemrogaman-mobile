# Pengenalan Flutter

## Sejarah flutter

Flutter merupakan *software development kit* (**sdk**) yang dikembangkan oleh google. Awalnya **sdk** ini ber nama **"sky"** dan dijalankan di OS android. Diresmikan pada saat KTT pengembangan dart, dengan tujuan bisa merender 120 fps secara konsisten.

Pada tahun 2017, dia me-release secara public versi alpha 0.0.6.

Pada tahun 2018, flutter me-release versi 1.0 yang memungkinan dapat digunakan pada banyak platform, yaitu desktop, web, dan mobile. Namun secara stable, flutter masih digunakan dalam platform mobile, yaitu **android** dan **ios**.

Pada tahun 2021, flutter me-release versi stable 2.0 untuk penggunaan **null-safety** pada dart **SDK**, dan meluncurkan versi stable untuk web.

## Arsitektur Flutter

Flutter memiliki arsitektur yang lumayan keren gan. Dia mengelompokkan beberapa layer. Mungkin bisa dilihat pada gambar di bawah ya. Kalau ingin lihat gambar yang tidak pecah silakan langsung ke sumbernya. Sudah ane cantumkan di bawah gambar.

![Architectural diagram](img\archdiagram.png)

## Text Editor

Tidak ada batasan **IDE** atau **text editor** tertentu yang dapat digunakan untuk **ngoding** flutter. Kita bebas memilih. **TAPI**, jika ente **tidak ingin ribet**, lebih baik install **android studio,** why? because ini akan mempermudah kita dalam proses installasi. Terutama untuk emulator android pun **android sdk**-nya.

## Keunikan Flutter

Flutter merupakan tool yang unik, faktanya:

- Flutter dapat membuat aplikasi **cross platform** yang bisa berjalan dengan mulus atau bahasa gaulnya (**smooth**). Keunikannya, kita bisa menghubungkan kode dart ke kode native platform tertentu. Misal ente membutuhkan koneksi bluetooth yang hanya digunakan di android, so menambah permission atau semacamnya dapat kita hubungkan dengan bahasa kottlin.
- Flutter berbasis **widget**, jadi **almost everything is widget**, bahkan ente bisa membuat widget sendiri.
- **Object oriented UI**, flutter tidak menggunakan **Markup Language.** So, ini juga menjadi salah satu faktor utama kenapa flutter begitu cepat.

# Hal Mendasar yang Wajib Diketahui

## 1. Object Oriented Programming

Flutter memiliki keunikan tersendiri, yaitu **almost everything is widget,** dan widget itu sendiri merupakan sebuah **object** yang direpresentasikan di bahasa pemrograman **dart**. Jadi, jika kita ingin belajar flutter, sangat dianjurkan untuk belajar **Object Oriented Programming (OOP)** bahasa pemrograman **dart**. Sebab, akan sangat sulit untuk memahami bagaimana flutter merender **UI** jika kita tidak tau OOP.

## **2. Widget**

Widget adalah komponen dasar pada flutter, dan memang ini ditekankan oleh flutter. Disetiap widget, selalu ada **context** yang merepresentasikan dimana widget berada.

## **3. Basic Design**

Jika sudah mempelajari OOP Dart, kita wajib mengetahui dasar-dasar pembuatan design. Jika kita memiliki pengalaman dalam web design, atau pengalaman dalam pembuatan UI & UX design seperti pada aplikasi figma dan sejenisnya. Maka sebagian besar pasti akan mudah dimengerti. Disini saya membuat list untuk dasar-dasar pembuatan design yang wajib kita ketahui.

- **Color**

  *Color* atau warna dituliskan dengan menggunakan kode. Penulisan warna dalam pemrograman ada beberapa macam. tolong disimak ya.

  1. Hexadecimal - penulisan warna ini lebih sering digunakan di aplikasi untuk design, pun web programming. Contoh penulisan **#000000** yang akan menghasilkan warna hitam, dan **#FFFFFF** yang akan menghasilkan warna putih. Dalam penulisan hexa decimal, huruf maksimal pasti hanya sampai dengan Huruf **F**, sedangkan angkanya antara 0 sampai 9. Panjang dari hexadecimal ini adalah 6 karakter.

  2. RGB - Red Green Blue, penulisan warna ini tersusun dari Merah Hijau dan Biru. Setiap parameter dari ketiga warna tersebut direpresentasikan dengan angka 0 hingga 255

  3. RGBA - Red Green Blue Alpha, sama dengan RGB namun alpha disini digunakan untuk merepresentasikan transparency warna, nilai Alpha biasanya 0 sampai dengan 1.

  Nah, hal-hal diatas merupakan beberapa cara untuk menulis warna pada web design. Untuk flutter bagaimana? Flutter memiliki object sendiri yang bernama **Color()** object ini hanya menerima parameter integer, jadi penulisan warna pada flutter:

  1. Hexa - **Color(0xff000000)** akan menghasilkan warna hitam.

  2. Untuk opacity atau alpha kita bisa menggunakan **.withOpacity()** lalu masukkan angka transparency-nya.

- **Typography**

  Typography merupakan metode seni penulisan yang dikombinasikan dalam bahasa pemrograman, bahasa markup, cascading, dan lain sebagainya yang bersangkutan dengan teknis. Hal dasar dalam typography yang wajib kita ketahui.

  1. Font family - Jenis huruf design aktual yang merepresentasikan tulisan.
  2. Font size - Ukuran font
  3. Font weight - Ketebalan font
  4. Font color - Warna font
  5. Font style - Biasanya ini terdiri atas underline , dan ~~strike~~
  6. Text alignment - penjajaran text, biasanya terdiri dari, **center, justify, right/end, start/left**
  7. Text direction - dari kiri ke kanan atau kanan kekiri
  8. Line height - tinggi baris disetiap paragraph
  9. Letter spacing - atau spasi per huruf.
  10. Heading - Representasi dari judul sebuah paragraph, heading/judul ini biasanya memiliki komposisi 1 sampai 6.
  11. Paragraph
  12. Span - atau bisa dibilang inline text

- **Box**

  Kenapa wajib mengetahui box? setiap UI memiliki **box-box** untuk diimplementasikan. Box biasanya digunakan dalam pembuatan **button** (tombol), pembungkus suatu object, meletakkan text, dan masih banyak lagi. Tentunya **box** merupakan element yang sangat penting untuk diketahui.

  Contoh box:

  ![image-20220311093631064](img\image-20220311093631064.png)

  Kotak doang kan. Selanjutnya kita bisa mengoles dengan **border radius**. seperti ini contohnya

  ![image-20220311093648491](img\image-20220311093648491.png)

  Sudah kelihatan ya border radius seperti apa. Lalu ada lagi hal dasar yang wajib diketaui dalam basic design. Yaitu, **box shadow.** Contohnya:

  ![image-20220311093703826](img\image-20220311093703826.png)

  Sudah terlihat ya, box shadow itu bayangan. Ini juga sangat penting nantinya.

## 4. Library Manager

Jika kita belum mengetahui apa itu library, library merupakan kumpulan kode untuk membantu pekerjaan kita. Nah, flutter ini memiliki ***library manager\*** sendiri namanya [pub.dev](https://pub.dev/) . Cara installasi library-nya pun tidak ribet. Nanti kita akan lanjut pada tulisan artikel selanjutnya ya. disini cuma pengetahuan saja. agar tidak terlalu banyak tulisan, mengurangi kebosanan membaca, dan agar web ini tidak terlalu banyak merender text, hihi.

## 5. State Management

Flutter tidak berbasis pada **Document Object Model** **(DOM)**, melainkan berbasis pada state manager. So, buat kita yang memiliki background web developer dan tidak ada pengalaman pada react, dan beberapa framework javascript yang menggunakan state manager tentunya harus beradaptasi lagi. Namun, dibandingkan DOM, **state** lebih simple dan mudah di maintain. State digunakan sebagai global variable yang men-triger perubahan UI ketika.

# Perbedaan StatelessWidget dengan StatefulWidget

Jika kita mengenal **react.js** , react.js memiliki hal mendasar yaitu component. Sedangkan flutter sendiri memiliki hal mendasar yang dinamakan **widget**. Widget merupakan sebuah object yg akan di jalankan oleh framework flutter untuk membangun sebuah *user interface.*

Dalam flutter, kita bisa membuat custom widget dengan kerangka yangg berisikan widget-widget yang kita inginkan. Dan disini kita akan membahas dua tipe custom widget yang wajib kita ketahui untuk membuat sebuah UI, yaitu **SatelessWidget** dan **StatefulWidget**.

## Penjelasan StatelessWidget

StatelessWidget adalah sebuah custom widget untuk membuat UI dengan attribute-attribute yang tidak bisa berubah/bermutasi (immutable). Mari kita coba contoh penggunaan statelessWidget, dan silakan pencet tombol run untuk melihat hasilnya

```dart
import 'package:flutter/material.dart';

const Color darkBlue = Color.fromARGB(255, 18, 32, 47);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: darkBlue,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: MyWidget(),
        ),
      ),
    );
  }
}

class MyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      'Hello, World!',
      style: Theme.of(context).textTheme.headline4,
    );
  }
}

```

Untuk lebih jelasnya, kita menjalankan kode tersebut pada [Codepen](https://codepen.io/)

Pada contoh kode diatas. kita dapat melihat hasilnya adalah

> hello world

hasil dari UI yang dirender pada kodingan diatas, tidak bisa dirubah oleh event apapun, sebab tidak ada **state** yang dapat merender ulang UI.

## Penjelasan StatefulWidget

Kali ini kita akan membahas apa itu StatefullWidget, kenapa perlu dan bagaimana cara penggunaannya.

StatefullWidget merupakan sebua object class yang diapat kita gunakan untuk membuat User Interface (antarmuka) yang dinamis dan dapat berubah-ubah bedasarkan event dan keinginan kita. Mari kita lihat contoh di bawah.

```dart
// Copyright (c) 2019, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  const MyHomePage({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

```

Untuk lebih jelasnya, kita menjalankan kode tersebut pada [Codepen](https://codepen.io/). Nah, pada contoh kode diatas, jika kita run, kita dapat membuat sebuah UI yang bisa berubah bedasarkan button **+** , untuk menambah nilai yang ditengah. Silakan dicoba.

mari kita lihat penjelasan gambar di bawah dan kita bedah kodenya.

![img](img\4bd9b371809f28d23b3e87fadfab09ae.png)

Penjelasan (poin penting ada pada nomor 5 sampai 14)

1. `runApp(MyApp())` :Merupakan sebuah method untuk menjalankan aplikasi kita
2. `StateLessWidget`: Merupakan salah satu turunan widget yg statis.
3. `Widget build(BuildContext context)` : Merupakan method yg selalu dipanggil oleh framework flutter untuk merender UI dalam anak turunan widget
4. `MaterialApp()`: Merupakan salah satu widget yang disediakan flutter untuk membuat sebuah aplikasi material.
5. `class HomePage extends StatefulWidget` : Merupakan custom widget kita untuk membuat class **StatefulWidget**.
6. `final String title` : Merupakan parameter title yang diisi oleh constructor
7. `const MyHomePage({})` : Constructor
8. `createState()` : Merupakan lifecycle yang wajib ditulis untuk membuat **StatefulWidget**.
9. `_MyHomePageState extends State` : Merupakan object / class untuk kita mulai membuat UI dan mengoperasikan State
10. `void _incrementCounter()` : Merupakan method untuk menambah angka +1 disitu ada setState() yang merupakan cara untuk merender ulang UI.
11. `Scaffold()` : Merupakan salah satu widget yang disediakan flutter lebih lengkapnya [disini](https://api.flutter.dev/flutter/material/Scaffold-class.html)
12. `widget.title()` : Merupakan cara untuk pengambilan parameter yang dilempar ke **StatefulWidget**.
13. `widget.title()` : Merupakan event klik pada tombol **+**

# Instalasi Flutter dengan Vs Code

Pastikan VSCode sudah terinstal pada komputer kita. Dengan menggunakan plugin Flutter official dan Dart, teks editor vscode bisa kita sulap untuk membuat dan membangun project flutter.

## 1. Buka VSCode

Langkah yang pertama adalah buka aplikasi VSCode.

![image-20220311095304124](img\image-20220311095304124.png)

## 2. Buka Menu Extensions

Anda bisa membuka menu extensions dengan menekan tombol `Ctrl + Shift + X` atau dengan langsung mengklik tombol extensions di panel menu sebelah kiri.

## 3. Install Flutter & Dart Plugin

Setelah itu ketik “Flutter” pada kotak pencarian.

![image-20220311095413185](img\image-20220311095413185.png)

Klik hasil yang paling atas.

Klik tombol **install**.

Tunggu sampai proses selesai.

Ketika kita memasang plugin flutter, harusnya plugin dart juga akan terinstall secara otomatis. Tapi untuk memastikan lebih jauh, anda bisa mencari plugin Dart dan lihat apakah sudah terinstall atau tidak.

![image-20220311095541355](img\image-20220311095541355.png)

## 4. Pastikan flutter telah terinstall tanpa error

Untuk memastikan apakah plugin flutter sudah working terpasang tanpa error:

1. Tekan shortcut `ctrl + shift + p`

2. Ketik `"Flutter"`

3. Jika kita mendapatkan hasil seperti ini:

   ![image-20220311095625735](img\image-20220311095625735.png)

# Cara Menginstall dan Menghubungkan Nox dengan VSCode

## 1. Cara Menginstall Nox

Pada tutorial ini penulis menggunakan OS windows 7 untuk menginstall Nox. Langsung saja cara menginstall nox yaitu :

### a. Download Nox

![img](img\nox1.png)

- Hubungkan komputer/ laptop ke internet **kemudian download Nox App Player dari situs resminya. Linknya yaitu** : https://www.bignox.com/
- **Klik tombol unduh** pada situs resmi nox yang sudah terbuka
- Tunggu sampai unduhan nox selesai

### b. Install Nox

- Jika unduhan sudah selesai **klik open atau install file exe nox**

![img](img\nox2.png)

- **Klik I have read and accept the Nox license agreement**, lalu **klik install**

![img](img\nox3.png)

- **Tunggu proses installasi** sampai 100 % kemudian **klik start** jika sudah

![img](img\nox4.png)

![img](img\nox5.png)

- Setelah loading, maka akan terbuka tampilan awal dari Emulator Nox

![img](img\nox6-1024x599.png)

### ![img](img\nox7.png)

### c. Ubah Tampilan

- Untuk mengubah tampilan Nox menjadi mode smartphone / mobile phone, caranya yaitu **klik System Settings** 

![img](img\nox7-Copy.png)

- **Pilih Advanced Settings,** ubah **startup settings** menjadi **mobile phone** 

![cara menginstall dan menghubungkan nox dengan VSCode](img\nox9.png)

- **Klik save settings**, **klik oke** jika diminta untuk **me restart Nox.** Setelah restart maka tampilan nox akan menjadi tampilan smartphone bukan tablet lagi.

![cara menginstall dan menghubungkan nox dengan VSCode](img\nox11.png)

## 2. Menghubungkan Nox dengan VSCode

Selanjutnya kita akan **menyeting Nox App Player** agar bisa terhubung dengan VSCode. Caranya yaitu :

### a. Setting Boot Root dan Bahasa

- Pastikan Nox App Player sudah terbuka
- **Pilih System Settings** seperti langkah pertama pada **bahasan C. ubah tampilan**
- **Ceklis root** pada **root startup**
- **Untuk languagenya** sendiri pilih **bahasa indonesia**, lalu pilih **save settings**

![img](img\nox12.png)

- **Restart** jika **Nox** meminta untuk **restart**

![img](img\nox13.png)

### b. Aktifkan Mode Pengembang

- Setelah proses restart selesai dan Nox App Player sudah terbuka kembali **pilih setting (terdapat pada folder tools)**

![img](img\nox14.png)

- **Scroll ke bawah** cari menu **tentang tablet**

![img](img\nox17.png)

- **Ketuk atau klik 7 kali pada nomor bentukan**, sehingga muncul notifikasi “*sekarang anda adalah seorang pengembang “*

![img](img\nox16.png)

- Silahkan **kembali lagi ke menu utama setelan**, lalu **pilih opsi pengembang**

![img](img\nox17-1.png)

- **Ceklis Debugging USB**, **klik oke** pada jendela **izinkan melakukan debug USB**

![img](img\nox18.png)

![img](img\nox19.png)

- Sekarang **debugging USB sudah aktif**

### c. Panggil di CMD dan Menghubungkan dengan VSCode

1. Pertama kita buka VSCode kemudian buat file baru, ketikan kode berikut:

   ```
   c:
   cd\
   cd C:\Program Files\Nox\bin
   start /b Nox.exe
   timeout 5
   nox_adb.exe connect 127.0.0.1:62001
   ```

   Simpan pada desktop, dengan nama **android_mulai.bat**. Pehatikan direktori penyimpanan aplikasi Nox kita.

2. Buat file baru, ketikan kode berikut:

   ```
   c:
   cd\
   cd C:\Program Files\Nox\bin
   timeout 5
   nox_adb.exe kill-server
   ```

   Simpan pada desktop, dengan nama **android_selesai.bat**. Pehatikan direktori penyimpanan aplikasi Nox kita.

3. Untuk menjalankan emulator, kita klik 2x pada  **android_selesai.bat** kemudian klik 2x pada  **android_mulai.bat**. Maka emulator android akan otomatis terbuka. 

   ![image-20220311102028892](img\image-20220311102028892.png)

> Nb. Perhatikan!
>
> **Minimum requirements:**
>
> - OS: Windows 7 SP1 64-bit, Windows 8.1 64-bit, Windows 10 64-bit, Windows 11 64-bit
> - Free storage: 1.64 GB
> - Pre-installed tools:
>   - Windows PowerShell 5.0 or newer
>   - Git 2.x
>
> **Recommend specs**
>
> - OS: Windows 10 64-bit
> - CPU: Intel Core i5-8400
> - Memory: 16 GB RAM
> - Free storage: 20 GB SSD
> - Tools: Windows PowerShell 5.0+, Git 2.x

# Install SDK Flutter

1. Buka link berikut: https://docs.flutter.dev/get-started/install/windows

   ![image-20220311102702088](img\image-20220311102702088.png)

   Download **flutter_windows_2.10.3-stale.zip**, kemudian baca **Peringatan!**

2. Kemudian Extract file yang sudah didownload pada drive C: seperti digambar.

3. Update path komputer

   - Klik Windows + E > Klik kanan This PC > Properties

   - Klik Advance system settings
   
     ![image-20220311104305093](img\image-20220311104305093.png)
   
   - Masuk Tab Advanced
   
     ![image-20220311104355376](img\image-20220311104355376.png)
   
   - Klik Path > Klik Edit
   
     ![image-20220311104447496](img\image-20220311104447496.png)
   
   - Klik Browse.. arahkan ke drive `C:\flutter\bin` Kemudian Ok
   
     ![image-20220311104523853](img\image-20220311104523853.png)
   
   - Klik Path (System Variable) > Klik Edit
   
   - ![image-20220311104639057](img\image-20220311104639057.png)
   
   - Klik Browse.. arahkan ke drive `C:\flutter\bin` Kemudian Ok
   
     ![image-20220311104805122](img\image-20220311104805122.png)
   
   - Klik Ok semua pada Window yang terbuka, kemudian Restart PC

# Install Android Studio

1. Buka [Android Studio](https://developer.android.com/studio) download dan install sampai selesai.

2. Buka Android Studio, kemudian selesaikan setting awal

   ![image-20220311125623029](img\image-20220311125623029.png)

   ![image-20220311125703935](img\image-20220311125703935.png)

3. Selanjutnya Konfiramasi Flutter untuk tempat instalasi android studio. 

   `flutter config --android-sdk="C:\Users\HP\AppData\Local\Android\Sdk"`

   `flutter config --android-studio-dir="C:\Program Files\Android\Android Studio"`

   Perhatikan direktori penyimpanan!

   kemudian buka cmd, ketik `flutter doctor`

   ![image-20220311131702985](img\image-20220311131702985.png)

   Usahakan semua sudah tanpa issue.

4. Buka Aplikasi Android Studio, Settings SDK 

   ![image-20220311142733320](img\image-20220311142733320.png)

5. Jika sudah selesai, kita setting visual studio nya. download https://visualstudio.microsoft.com/downloads/ ENterprise 2022

6. Jika sudah selesai buka aplikasinya, kemudian masuk ke tab avaible. Klik install

   ![image-20220311143048161](img\image-20220311143048161.png)

   ![image-20220311143209534](img\image-20220311143209534.png)

7. Install!

# Test Drive Flutter Di VSCode

Langkah-langkahnya adalah:

1. Ketik shortcut `ctrl + shift + p`

2. Ketik **Flutter**

3. Pilih menu **Flutter > New Application Project**

   ![image-20220311131936231](img\image-20220311131936231.png)
   
   Kemudian Enter, selanjutnya pilih Application seperti digambar
   
   ![image-20220311132027149](img\image-20220311132027149.png)

3. Pilih Folder penyimpanan projek flutter

   ![image-20220311132135394](img\image-20220311132135394.png)

4. Masukkan nama projek 

   ![image-20220311132255595](img\image-20220311132255595.png)

   ![image-20220311132352377](img\image-20220311132352377.png)

5. Centang, kemudian yes.

6. Edit **lib/main.dart** menjadi seperti ini.

   ```dart
   import 'package:flutter/material.dart';
   
   void main() {
     runApp(const MyApp());
   }
   
   class MyApp extends StatelessWidget {
     const MyApp({Key? key}) : super(key: key);
   
     // This widget is the root of your application.
     @override
     Widget build(BuildContext context) {
       return MaterialApp(
         title: 'Jor Flutter',
         theme: ThemeData(
           // This is the theme of your application.
           //
           // Try running your application with "flutter run". You'll see the
           // application has a blue toolbar. Then, without quitting the app, try
           // changing the primarySwatch below to Colors.green and then invoke
           // "hot reload" (press "r" in the console where you ran "flutter run",
           // or simply save your changes to "hot reload" in a Flutter IDE).
           // Notice that the counter didn't reset back to zero; the application
           // is not restarted.
           primarySwatch: Colors.blue,
         ),
         home: const MyHomePage(title: 'Home Page'),
       );
     }
   }
   
   class MyHomePage extends StatefulWidget {
     const MyHomePage({Key? key, required this.title}) : super(key: key);
   
     // This widget is the home page of your application. It is stateful, meaning
     // that it has a State object (defined below) that contains fields that affect
     // how it looks.
   
     // This class is the configuration for the state. It holds the values (in this
     // case the title) provided by the parent (in this case the App widget) and
     // used by the build method of the State. Fields in a Widget subclass are
     // always marked "final".
   
     final String title;
   
     @override
     State<MyHomePage> createState() => _MyHomePageState();
   }
   
   class _MyHomePageState extends State<MyHomePage> {
     int _counter = 0;
   
     void _incrementCounter() {
       setState(() {
         // This call to setState tells the Flutter framework that something has
         // changed in this State, which causes it to rerun the build method below
         // so that the display can reflect the updated values. If we changed
         // _counter without calling setState(), then the build method would not be
         // called again, and so nothing would appear to happen.
         _counter++;
       });
     }
   
     @override
     Widget build(BuildContext context) {
       // This method is rerun every time setState is called, for instance as done
       // by the _incrementCounter method above.
       //
       // The Flutter framework has been optimized to make rerunning build methods
       // fast, so that you can just rebuild anything that needs updating rather
       // than having to individually change instances of widgets.
       return Scaffold(
         appBar: AppBar(
           // Here we take the value from the MyHomePage object that was created by
           // the App.build method, and use it to set our appbar title.
           title: Text(widget.title),
         ),
         body: Center(
           // Center is a layout widget. It takes a single child and positions it
           // in the middle of the parent.
           child: Column(
             // Column is also a layout widget. It takes a list of children and
             // arranges them vertically. By default, it sizes itself to fit its
             // children horizontally, and tries to be as tall as its parent.
             //
             // Invoke "debug painting" (press "p" in the console, choose the
             // "Toggle Debug Paint" action from the Flutter Inspector in Android
             // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
             // to see the wireframe for each widget.
             //
             // Column has various properties to control how it sizes itself and
             // how it positions its children. Here we use mainAxisAlignment to
             // center the children vertically; the main axis here is the vertical
             // axis because Columns are vertical (the cross axis would be
             // horizontal).
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               const Text(
                 'You have pushed the button this many times:',
               ),
               Text(
                 '$_counter',
                 style: Theme.of(context).textTheme.headline4,
               ),
             ],
           ),
         ),
         floatingActionButton: FloatingActionButton(
           onPressed: _incrementCounter,
           tooltip: 'Increment',
           child: const Icon(Icons.add),
         ), // This trailing comma makes auto-formatting nicer for build methods.
       );
     }
   }
   ```

7. Kemudian kita buka file **android_selesai.bat** dan **android_mulai** (untuk memulai emulator).

8. Jika Nox emulator sudah terbuka, maka kita tinggal memilih Nox Emulator sebagai peramban untuk menjalankan program. Klik kotak kecil bawah, pilih **SM G965N** (nama emulator)

   Atau kita bisa pilih **Chrome** supaya komputer kita lebih ringan.

   ![image-20220311133552765](img\image-20220311133552765.png)

9. Kemudian tekan F5

10. Buka projek kita pada Nox Player

    ![image-20220311134244698](img\image-20220311134244698.png)

    11. **Edit Kode + Testing Hot Reload**

        Di antara kelebihan ngoding dengan flutter adalah fitur hot reload. Fitur ini sendiri adalah fitur yang sangat umum beberapa tahun belakangan (terlebih lagi di dunia javascript). Dengan hot reload, hasil edit yang kita lakukan pada kode program, akan langsung terlihat hasilnya tanpa melakukan proses kompilasi dari awal.

        Dengan ini, proses development menjadi jauh lebih cepat ⚡⚡⚡.

        Langsung saja, kita akan mengedit tulisan `"You have pushed the button this many times"` menjadi bahasa Indonesia.

        Jika versi template-nya sama, kalian bisa menemukan kode tersebut pada baris `97` seperti tangkapan layar di bawah:

        ![image-20220311134420453](img\image-20220311134420453.png)

        Ganti menjadi `"kita sudah menekan tombol sebanyak:"`.

        ![image-20220311134705913](img\image-20220311134705913.png)

        Save hasil edit dengan cara menekan shortcut **Ctrl+S**.

        Dan dalam sekejab, hasil editan kita langsung tampil pada device yang sedang berjalan.

        > Nb.
        >
        > Untuk membersihkan emulator, kita bisa masuk ke folder projeknya. Kemudian ketik **flutter clean**

