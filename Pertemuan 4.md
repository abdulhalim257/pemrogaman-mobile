# **Custom Font pada Flutter**

1. Jadi pertama-tama, kita download dulu ya, Font yang akan kita gunakan di https://fonts.google.com/, pilih Poppins. Kemudian klik Download Family

   ![image-20220331092656813](img\image-20220331092656813.png)

2. Selanjutnya Copy Paste font yang sudah didownload seperti pada digambar.

   ![image-20220331092830482](img\image-20220331092830482.png)

3. Atur file **pubspec.yaml** seperti berikut. 

   **Sesuaikan nama folder, nama font nya dan posisi tulisan! (tab)**

   ![image-20220331094214510](img\image-20220331094214510.png)

4. Buat file baru pada folder **lib**

   ![image-20220331093400938](img\image-20220331093400938.png)

5. Edit **font_style.dart** seperti berikut

   ```dart
   import 'dart:ui';
   
   import 'package:flutter/material.dart';
   
   TextStyle mainHeader =
       TextStyle(fontSize: 26, color: Colors.black38, fontFamily: 'Poppins');
   ```

6. Menggunakan **font_style.dart** pada **main.dart**. Ubah file main.dart:

   ```dart
   import 'package:flutter/material.dart';
   import 'font_style.dart';
   ```

   kita rubah tulisan `Rich Together` dengan style font kita.

   ```dart
   Text("Rich Together", style: mainHeader,),
   ```

7. Kita jalankan dengan menakan **F5**

   ![image-20220331094315074](img\image-20220331094315074.png)

8.  kita Edit **font_style.dart** seperti berikut untuk merubah warna **mainFont**

   ```dart
   Color.fromARGB(255, 0, 165, 248),
   ```

   maka Hasilnya , 

   ***Jangan lupa Hot Reload!**

   ![image-20220331094759473](img\image-20220331094759473.png)

9. buat style untuk sub header nya

   ```dart
   TextStyle subHeader =
       TextStyle(fontSize: 16, color: Colors.blueGrey, fontFamily: 'Poppins');
   ```

10. kita rubah tulisan `Save your money little bit and we will help to be rich` pada **main.dart** dengan style font **subHeader**.

    ```dart
    Text(
        "Save your money little bit and we will help to be rich",
        textAlign: TextAlign.center,
        style: subHeader,
    )
    ```

    Hasilnya,

    ![image-20220331095314158](img\image-20220331095314158.png)

11. Selanjutnya kita coba memberikan margin/padding pada objek,

    Ubah file **main.dart**

    ![image-20220331100140718](img\image-20220331100140718.png)

    ```dart
    padding: EdgeInsets.only(left: 0, top: 50, right: 0, bottom: 0),
    ```

    Hasilnya

    ![image-20220331100200924](img\image-20220331100200924.png)

# Row dan Column

Row dan Column Flutter (baris dan kolom Flutter) adalah dua widget penting dalam pemrograman Dart di framework Flutter. Widget ini memungkinkan Anda menyelaraskan *child widget* secara horizontal dan vertikal sesuai kebutuhan. Seperti yang kita ketahui bahwa ketika kita mendesain UI (User Interface) apa pun dalam Flutter, kita perlu mengatur kontennya secara row dan column sehingga widget Baris dan Kolom ini diperlukan saat mendesain UI.

### Contructor Row Flutter

Berikut ini adalah contoh konstruktor dari Row Class:

```dart
Row(
{Key key,
MainAxisAlignment mainAxisAlignment: MainAxisAlignment.start,
MainAxisSize mainAxisSize: MainAxisSize.max,
CrossAxisAlignment crossAxisAlignment: CrossAxisAlignment.center,
TextDirection textDirection,
VerticalDirection verticalDirection: VerticalDirection.down,
TextBaseline textBaseline: TextBaseline.alphabetic,
List<Widget> children: const <Widget>[]}
)
```

### Constructor Column Flutter

```dart
Column(
{Key key,
MainAxisAlignment mainAxisAlignment: MainAxisAlignment.start,
MainAxisSize mainAxisSize: MainAxisSize.max,
CrossAxisAlignment crossAxisAlignment: CrossAxisAlignment.center,
TextDirection textDirection,
VerticalDirection verticalDirection: VerticalDirection.down,
TextBaseline textBaseline,
List<Widget> children: const <Widget>[]}
)
```

### Properti Widget Row dan Column Flutter

Berikut ini adalah properti yang ada di dalam Row dan Column Flutter

- **Child***:* properti ini mengambil di dalam *List<Widget>,* yaitu daftar widget untuk ditampilkan di dalam widget row dan column.
- **clipBehaviour***:* properti ini memegang kelas klip sebagai objek untuk memutuskan yang mana konten di dalam row atau column harus diklip atau tidak.
- **crossAxisAlignment:** crossAxisAlignment mengambil crossAxisAlignment enum, sebagai objek bagaimana child widget harus ditempatkan di crossAxisAlignment. Untuk row itu vertikal dan untuk column itu horizontal.
- **direction:** properti ini memegang row dan column sebagai object Axis enum untuk memutuskan **direction** yang digunakan di dalam sumbu utama. Dalam row dan column hal ini pun sudah dipastikan.
- **mainAxisAlignment:** properti ini mengambil enum MainAxisAlignment sebagao objek untuk memutuskan bagaimana para child widget akan ditempatkan di dalam mainAxisAlignment. Untuk row ia ditempatkan horizontal, dan column ditempatkan vertikal.
- **mainAxisSize:** properti ini memutuskan ukuran dari sumbu utama yang diambil dalam MainAxisSize enum sebagai objek.
- **runtimeType:** properti ini memberitahukan tipe runtime dari widget row atau column.
- **textBaseline:** properti ini bertanggungjawab untuk menyelaraskan teks di dalam widget row atau colum dengan masing-masing ke sebuah baseline.
- **textDirection:** properti ini mengontrol arah teks dari widget row atau column yang mana bisa dari kiri ke kanan secara default atau sebaliknya.
- **verticalDirection:** properti ini mengambil di dalam VerticalDirection enum sebagai objek untuk menentukan child widget yang harus di layer.

### Flutter Row

Flutter row membuat sebuah array horizontal dari *child.* Anda dapat menyelaraskan konten sesuai pilihan kami dengan menggunakan mainAxisAlignment dan crossAxisAlignment. Main axis row adalah horizontal dan sumbu silang ke Main axis row adalah vertikal. Kita bisa menyelaraskan anak secara horizontal menggunakan MainAxisAlignment dan secara vertikal menggunakan CrossAxisAlignment di baris itu.

Terlepas dari mainAxisAlignment dan crossAxisAlignment ini, kami juga memiliki beberapa properti lain seperti mainAxisSize,textDirection,verticalDirection dll. tetapi kami akan fokus pada dua ini (mainAxisAlignment dan crossAxisAlignment).

Kita dapat melakukan penyelarasan konten dengan menggunakan properti di bawah ini:

- start : Tempatkan para *child* dari awal baris.
- end : Tempatkan para *child* di akhir baris.
- center : Tempatkan para *child* di tengah barisan.
- spaceBetween : Tempatkan ruang secara merata di antara *child*.
- spaceAround : Tempatkan ruang secara merata di antara *child* dan juga setengah dari ruang itu sebelum dan sesudah *child* pertama dan terakhir.
- spaceEvenly : Tempatkan ruang secara merata di antar *child* dan juga sebelum dan sesudah *child* pertama dan terakhir.

Selanjutnya penerapannya pada projek kita adalah sebagai berikut:

```dart
body: SafeArea(
          child: Container(
            margin: EdgeInsets.all(40),
            padding: EdgeInsets.only(left: 0, top: 50, right: 0, bottom: 0),
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/images/gambar.png'),
                      height: 200,
                    ),
                    Text(
                      "Rich Together",
                      style: mainHeader,
                    ),
                    Text(
                      "Save your money little bit and we will help to be rich",
                      textAlign: TextAlign.center,
                      style: subHeader,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
```

Pada kode diatas kita sudah memasukkan **Column** pada Widgets **Row**. Kemudian kita beri `mainAxisAlignment` pada konten **Row**

![image-20220331101335236](img\image-20220331101335236.png)

```dart
mainAxisAlignment: MainAxisAlignment.center,
```

Kemudian kita juga beri `mainAxisAlignment` pada konten **Column**

![image-20220331101452341](img\image-20220331101452341.png)

Hasilnya

![image-20220331101633006](img\image-20220331101633006.png)

karena diawal tadi kita sudah memberi margin dan padding maka hasilnya akan seperti diatas, kemudian kita perbaiki lagi codenya menjadi seperti berikut

```dart
body: SafeArea(
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/images/gambar.png'),
                      height: 200,
                    ),
                    Text(
                      "Rich Together",
                      style: mainHeader,
                    ),
                    Text(
                      "Save your money little bit and we will \nhelp to be rich",
                      textAlign: TextAlign.center,
                      style: subHeader,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
```

# Custom Color Design System

1. Buat file **custom_colors.dart**

   ![image-20220331102210239](img\image-20220331102210239.png)

2. Ubah file tersebut menjadi seperti berikut

   ```dart
   import 'package:flutter/material.dart';
   
   // kode hex color
   Color darkBlue = Color(0XFF2c2c54);
   ```

3. Kemudian kita ubah **font_style.dart**

   ```dart
   TextStyle mainHeader =
       TextStyle(fontSize: 26, color: darkBlue, fontFamily: 'Poppins');
   ```

   bisa dipahami bahwa `color ` pada **mainHeader** sudah menggunakan **custom_colors.dart**

   ![image-20220331102740310](img\image-20220331102740310.png)

4. Kita coba import warna kita pada **main.dart**

   ```dart
   import 'package:flutter/material.dart';
   import 'font_style.dart';
   import 'custom_colors.dart';
   ```

    kita ubah background **AppBar** kita menjadi

   ![image-20220331103112600](img\image-20220331103112600.png)

   Hasilnya,

   ![image-20220331103145852](img\image-20220331103145852.png)